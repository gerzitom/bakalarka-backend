Nacházíte se v repozitáři aplikace Software pro vytváření školních testů, backend, ke stejnojmenné bakalářské práci. 

Inicializace dat:
- v adresáří db/db_init_scrips se nachází inicializační script init_db.sql
- přihlašovací údaje pro učitele jsou jméno: teacher, heslo: haha 
- ostatní uživatelé jsou studenti, přihlašovací jména jsou: max1, tall1, aysha1, marlon1. Hesla u všech uživatelů jsou také "haha"
- v init_db se také nachází jedna předpřipravená šablona, ze které lze rovnou generovat test pro celou třídu s výše zmíněnými studenty 

Běžící aplikace je také nasazena na adrese https://online-edu.aubrecht.net.
Adresa rozhraní na tvorbu testovacích šablon: https://online-edu.aubrecht.net/rest/test_templates.
