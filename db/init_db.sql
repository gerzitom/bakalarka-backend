INSERT INTO public.user_account (id, birthday, city, email, firstname, gender, password, phone, registered, role, street, surname, username, zip) VALUES (1, '1990-03-01 16:58:47.469000', 'město', 'admin@example.com', 'Daniel', 'male', 'jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=', '123456789', '2021-03-01 16:58:47.469000', 'admin', 'ulice', 'Koten', 'admin', 'psč');
INSERT INTO public.user_account (id, birthday, city, email, firstname, gender, password, phone, registered, role, street, surname, username, zip) VALUES (2, '1998-06-30 00:00:00.000000', 'Vizovice', 'tomas.gerzicak@gmail.com', 'Tomáš', 'male', 'CQsjXp648Zfy3ZJ5NyIsVwOW2XEiLZAJqRieK2zAosE=', '739175258', '2021-12-05 11:33:21.759000', 'teacher', 'Lhotsko 20', 'Geržičák', 'teacher', '76312');
INSERT INTO public.user_account (id, birthday, city, email, firstname, gender, password, phone, registered, role, street, surname, username, zip) VALUES (3, '1998-06-30 00:00:00.000000', 'Vizovice', 'identicon', 'Rudi', 'male', 'CQsjXp648Zfy3ZJ5NyIsVwOW2XEiLZAJqRieK2zAosE=', '739175258', '2021-12-05 11:33:21.759000', 'student', 'Lhotsko 20', 'Kirkland', 'rudi1', '76312');
INSERT INTO public.user_account (id, birthday, city, email, firstname, gender, password, phone, registered, role, street, surname, username, zip) VALUES (4, '1998-06-30 00:00:00.000000', 'Vizovice', 'identicon', 'Max', 'male', 'CQsjXp648Zfy3ZJ5NyIsVwOW2XEiLZAJqRieK2zAosE=', '739175258', '2021-12-05 11:33:21.759000', 'student', 'Lhotsko 20', 'Calhoun', 'max1', '76312');
INSERT INTO public.user_account (id, birthday, city, email, firstname, gender, password, phone, registered, role, street, surname, username, zip) VALUES (5, '1998-06-30 00:00:00.000000', 'Vizovice', 'identicon', 'Tallulah', 'male', 'CQsjXp648Zfy3ZJ5NyIsVwOW2XEiLZAJqRieK2zAosE=', '739175258', '2021-12-05 11:33:21.759000', 'student', 'Lhotsko 20', 'Bowes', 'tall1', '76312');
INSERT INTO public.user_account (id, birthday, city, email, firstname, gender, password, phone, registered, role, street, surname, username, zip) VALUES (6, '1998-06-30 00:00:00.000000', 'Vizovice', 'identicon', 'Aysha', 'male', 'CQsjXp648Zfy3ZJ5NyIsVwOW2XEiLZAJqRieK2zAosE=', '739175258', '2021-12-05 11:33:21.759000', 'student', 'Lhotsko 20', 'Chadwick', 'aysha1', '76312');
INSERT INTO public.user_account (id, birthday, city, email, firstname, gender, password, phone, registered, role, street, surname, username, zip) VALUES (7, '1998-06-30 00:00:00.000000', 'Vizovice', 'identicon', 'Marlon', 'male', 'CQsjXp648Zfy3ZJ5NyIsVwOW2XEiLZAJqRieK2zAosE=', '739175258', '2021-12-05 11:33:21.759000', 'student', 'Lhotsko 20', 'Bowes', 'marlon1', '76312');
SELECT setval('user_account_id_seq', 100);

INSERT INTO public.teacher (id, classroom, user_account) VALUES (1, null, 2);
SELECT setval('teacher_id_seq', 100);

INSERT INTO public.classroom (id, name, teacher) VALUES (1, '8.B', 1);
SELECT setval('classroom_id_seq', 100);

INSERT INTO public.student (id, chat_alert, points, total_points, classroom, user_account) VALUES (1, null, null, null, 1, 4);
INSERT INTO public.student (id, chat_alert, points, total_points, classroom, user_account) VALUES (2, null, null, null, 1, 4);
INSERT INTO public.student (id, chat_alert, points, total_points, classroom, user_account) VALUES (3, null, null, null, 1, 5);
INSERT INTO public.student (id, chat_alert, points, total_points, classroom, user_account) VALUES (4, null, null, null, 1, 6);
INSERT INTO public.student (id, chat_alert, points, total_points, classroom, user_account) VALUES (5, null, null, null, 1, 7);
SELECT setval('student_id_seq', 100);

-- CREATE TEST TEMPLATE
INSERT INTO public.template (id, name, author) VALUES (103, 'Ukázková šablona', 1);
SELECT setval('template_id_seq', 150);

-- TEST TEMPLATE PARTS
INSERT INTO public.templatepart (id, description, estimatedtime, name, points, preferredtype, template) VALUES (163, '', 10, 'Hlavní města států', 10, 'ONE_OPTION', 103);
INSERT INTO public.templatepart (id, description, estimatedtime, name, points, preferredtype, template) VALUES (164, '', 10, 'Města ve státech', 15, 'MULTIPLE_OPTION', 103);
INSERT INTO public.templatepart (id, description, estimatedtime, name, points, preferredtype, template) VALUES (166, '', 20, 'Hlavní města států spojování', 20, 'SPOJOVACKA', 103);
INSERT INTO public.templatepart (id, description, estimatedtime, name, points, preferredtype, template) VALUES (167, '', 10, 'Dodatečné otázky', 10, 'TEXT_AREA', 103);

SELECT setval('templatepart_id_seq', 200);

-- QUESTION GROUPS
INSERT INTO public.templatequestiongroup (id, grouptype, instructions, numberofgeneratedquestions, points, templatepart) VALUES (214, 'SIMPLE', 'Hlavní město UK', 0, 10, 163);
INSERT INTO public.templatequestiongroup (id, grouptype, instructions, numberofgeneratedquestions, points, templatepart) VALUES (215, 'SIMPLE', 'Hlavní město ČR', 0, 10, 163);
INSERT INTO public.templatequestiongroup (id, grouptype, instructions, numberofgeneratedquestions, points, templatepart) VALUES (216, 'SIMPLE', 'Hlavní město SR', 0, 10, 163);
INSERT INTO public.templatequestiongroup (id, grouptype, instructions, numberofgeneratedquestions, points, templatepart) VALUES (218, 'SIMPLE', 'Města v ČR', 0, 15, 164);
INSERT INTO public.templatequestiongroup (id, grouptype, instructions, numberofgeneratedquestions, points, templatepart) VALUES (219, 'SIMPLE', 'Města v UK', 0, 15, 164);
INSERT INTO public.templatequestiongroup (id, grouptype, instructions, numberofgeneratedquestions, points, templatepart) VALUES (221, 'SPOJOVACKA', 'Spojte stát a hlavní město', 0, 20, 166);
INSERT INTO public.templatequestiongroup (id, grouptype, instructions, numberofgeneratedquestions, points, templatepart) VALUES (222, 'OPEN', 'Napište co, jste se dozvěděli nového z geografie', 0, 10, 167);

SELECT setval('templatequestiongroup_id_seq', 250);

-- QUESTIONS
INSERT INTO public.templatequestion (id, answertype, content, points, questiongroup) VALUES (117, 'ONE_OPTION', '', 10, 214);
INSERT INTO public.templatequestion (id, answertype, content, points, questiongroup) VALUES (118, 'ONE_OPTION', '', 10, 215);
INSERT INTO public.templatequestion (id, answertype, content, points, questiongroup) VALUES (119, 'ONE_OPTION', '', 10, 216);
INSERT INTO public.templatequestion (id, answertype, content, points, questiongroup) VALUES (121, 'MULTIPLE_OPTIONS', '', 15, 218);
INSERT INTO public.templatequestion (id, answertype, content, points, questiongroup) VALUES (122, 'MULTIPLE_OPTIONS', '', 15, 219);
INSERT INTO public.templatequestion (id, answertype, content, points, questiongroup) VALUES (124, 'ONE_OPTION', 'ČR', 6.666666666666667, 221);
INSERT INTO public.templatequestion (id, answertype, content, points, questiongroup) VALUES (125, 'ONE_OPTION', 'SR', 6.666666666666667, 221);
INSERT INTO public.templatequestion (id, answertype, content, points, questiongroup) VALUES (126, 'ONE_OPTION', 'UK', 6.666666666666667, 221);
INSERT INTO public.templatequestion (id, answertype, content, points, questiongroup) VALUES (127, 'OPENANSWER', '', 10, 222);
SELECT setval('templatequestion_id_seq', 150);
-- OPTIONS
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (135, null, null, 'Londýn');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (136, null, null, 'Praha');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (137, null, null, 'Vídeň');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (138, null, null, 'Paříž');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (139, null, null, 'Praha');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (140, null, null, 'Londýn');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (141, null, null, 'pařiž');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (142, null, null, 'Vídeň');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (143, null, null, 'Bratislava');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (144, null, null, 'Londýn');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (145, null, null, 'Brno');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (146, null, null, 'Ostrava');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (147, null, null, 'Žilina');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (148, null, null, 'Zlín');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (149, null, null, 'Praha');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (150, null, null, 'Bratislava');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (151, null, null, 'Ostrava');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (154, null, null, 'Tokio');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (155, null, null, 'Londýn');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (156, null, null, 'Bratilava');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (157, null, null, 'Praha');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (158, null, null, 'Oslo');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (159, null, null, 'Tokio');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (160, null, null, 'Leeds');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (161, null, null, 'Edinburg');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (163, null, null, 'Praha');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (164, null, null, 'Bratislava');
INSERT INTO public.templateoption (id, additionalinfo, content, text) VALUES (165, null, null, 'Londýn');

SELECT setval('templateoption_id_seq', 200);

-- POSSIBLE ANSWERS
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (147, false, 147, 119);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (143, true, 143, 119);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (150, false, 150, 121);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (154, false, 154, 121);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (151, true, 151, 121);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (149, true, 149, 121);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (148, true, 148, 121);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (156, false, 156, 122);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (157, false, 157, 122);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (158, false, 158, 122);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (159, false, 159, 122);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (161, true, 161, 122);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (160, true, 160, 122);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (155, true, 155, 122);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (163, true, 163, 124);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (164, true, 164, 125);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (165, true, 165, 126);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (136, false, 136, 117);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (137, false, 137, 117);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (138, false, 138, 117);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (135, true, 135, 117);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (140, false, 140, 118);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (141, false, 141, 118);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (142, false, 142, 118);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (139, true, 139, 118);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (144, false, 144, 119);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (145, false, 145, 119);
INSERT INTO public.templatepossibleanswer (id, correct, option, question) VALUES (146, false, 146, 119);

SELECT setval('templatepossibleanswer_id_seq', 200);

--EXAM
INSERT INTO public.exam (id, duration, enddate, name, points, startdate, author, template) VALUES (105, 51, '2022-05-23 11:30:03.443000', 'Ukázkový test', 65, '2022-05-23 09:05:03.443000', 1, null);
SELECT setval('exam_id_seq', 150);
-- TEST
INSERT INTO public.test (id, teachermarkingrequired, classroom, exam) VALUES (121, false, 1, 105);
INSERT INTO public.test (id, teachermarkingrequired, classroom, exam) VALUES (122, false, 1, 105);
INSERT INTO public.test (id, teachermarkingrequired, classroom, exam) VALUES (123, false, 1, 105);
INSERT INTO public.test (id, teachermarkingrequired, classroom, exam) VALUES (124, false, 1, 105);
INSERT INTO public.test (id, teachermarkingrequired, classroom, exam) VALUES (125, false, 1, 105);
SELECT setval('test_id_seq', 150);
--TESTQUESTIONGROUP
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (136, 'OPEN', 'Napište vaše další znalosti', 5);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (137, 'SIMPLE', 'Hlavní město Velké Británie', 10);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (138, 'SPOJOVACKA', 'Hlavní města 1', 30);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (139, 'SIMPLE', 'Města ve Velké Británii', 20);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (140, 'OPEN', 'Napište vaše další znalosti', 5);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (141, 'SPOJOVACKA', 'Hlavní města 1', 30);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (142, 'OPEN', 'Napište vaše další znalosti', 5);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (143, 'SIMPLE', 'Města v ČR', 20);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (144, 'SIMPLE', 'Města v ČR', 20);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (145, 'SIMPLE', 'Hlavní město Velké Británie', 10);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (146, 'SPOJOVACKA', 'Hlavní města 1', 30);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (147, 'SIMPLE', 'Hlavní město ČR', 10);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (148, 'SIMPLE', 'Hlavní město Velké Británie', 10);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (149, 'SPOJOVACKA', 'Hlavní města 1', 30);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (150, 'SIMPLE', 'Města v ČR', 20);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (151, 'SIMPLE', 'Města ve Velké Británii', 20);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (152, 'SPOJOVACKA', 'Hlavní města 1', 30);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (153, 'OPEN', 'Napište vaše další znalosti', 5);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (154, 'OPEN', 'Napište vaše další znalosti', 5);
INSERT INTO public.questiongroup (id, grouptype, instructions, points) VALUES (155, 'SIMPLE', 'Hlavní město SR', 10);

SELECT setval('questiongroup_id_seq', 200);

-- TESTPART
INSERT INTO public.testpart (id, questiongroup, test) VALUES (136, 141, 124);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (137, 151, 123);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (138, 150, 124);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (139, 143, 122);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (140, 153, 124);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (141, 155, 124);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (142, 142, 123);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (143, 144, 121);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (144, 139, 125);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (145, 147, 123);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (146, 146, 122);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (147, 154, 122);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (148, 136, 125);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (149, 138, 123);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (150, 145, 121);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (151, 152, 125);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (152, 137, 122);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (153, 140, 121);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (154, 149, 121);
INSERT INTO public.testpart (id, questiongroup, test) VALUES (155, 148, 125);
SELECT setval('testpart_id_seq', 200);

--TESTQUESTION
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (166, 'OPENANSWER', '', 5, 136);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (167, 'OPENANSWER', '', 5, 154);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (168, 'ONE_OPTION', 'SR', 7.5, 152);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (169, 'MULTIPLE_OPTIONS', '', 20, 151);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (170, 'ONE_OPTION', 'Velká Británie', 7.5, 152);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (171, 'ONE_OPTION', 'SR', 7.5, 149);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (172, 'ONE_OPTION', 'ČR', 7.5, 141);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (173, 'ONE_OPTION', 'Rakousko', 7.5, 152);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (174, 'ONE_OPTION', 'SR', 7.5, 146);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (175, 'OPENANSWER', '', 5, 153);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (176, 'ONE_OPTION', 'Rakousko', 7.5, 138);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (177, 'ONE_OPTION', 'SR', 7.5, 141);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (178, 'ONE_OPTION', 'ČR', 7.5, 149);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (179, 'ONE_OPTION', 'ČR', 7.5, 146);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (180, 'ONE_OPTION', 'Rakousko', 7.5, 141);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (181, 'OPENANSWER', '', 5, 140);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (182, 'ONE_OPTION', 'Velká Británie', 7.5, 149);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (183, 'ONE_OPTION', 'Velká Británie', 7.5, 141);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (184, 'ONE_OPTION', 'Velká Británie', 7.5, 138);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (185, 'ONE_OPTION', 'Rakousko', 7.5, 146);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (186, 'MULTIPLE_OPTIONS', '', 20, 150);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (187, 'ONE_OPTION', '', 10, 137);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (188, 'OPENANSWER', '', 5, 142);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (189, 'MULTIPLE_OPTIONS', '', 20, 139);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (190, 'ONE_OPTION', 'ČR', 7.5, 138);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (191, 'ONE_OPTION', 'SR', 7.5, 138);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (192, 'MULTIPLE_OPTIONS', '', 20, 143);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (193, 'ONE_OPTION', 'ČR', 7.5, 152);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (194, 'ONE_OPTION', '', 10, 147);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (195, 'ONE_OPTION', '', 10, 145);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (196, 'ONE_OPTION', '', 10, 148);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (197, 'ONE_OPTION', '', 10, 155);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (198, 'ONE_OPTION', 'Rakousko', 7.5, 149);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (199, 'MULTIPLE_OPTIONS', '', 20, 144);
INSERT INTO public.question (id, answertype, content, points, questiongroup) VALUES (200, 'ONE_OPTION', 'Velká Británie', 7.5, 146);

SELECT setval('question_id_seq', 300);

--TESTOPTION
INSERT INTO public.option (id, additionalinfo, text) VALUES (192, null, 'Praha');
INSERT INTO public.option (id, additionalinfo, text) VALUES (193, null, 'Žilina');
INSERT INTO public.option (id, additionalinfo, text) VALUES (194, null, 'Bratislava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (195, null, 'Bratislava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (196, null, 'Leeds');
INSERT INTO public.option (id, additionalinfo, text) VALUES (197, null, 'Bratislava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (198, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (199, null, 'Vídeň');
INSERT INTO public.option (id, additionalinfo, text) VALUES (200, null, 'Košice');
INSERT INTO public.option (id, additionalinfo, text) VALUES (201, null, 'Brno');
INSERT INTO public.option (id, additionalinfo, text) VALUES (202, null, 'Bratislava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (203, null, 'Brno');
INSERT INTO public.option (id, additionalinfo, text) VALUES (204, null, 'Bratislava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (205, null, 'Praha');
INSERT INTO public.option (id, additionalinfo, text) VALUES (206, null, 'Leeds');
INSERT INTO public.option (id, additionalinfo, text) VALUES (207, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (208, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (209, null, 'Praha');
INSERT INTO public.option (id, additionalinfo, text) VALUES (210, null, 'Ostrava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (211, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (212, null, 'Bratislava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (213, null, 'Vídeň');
INSERT INTO public.option (id, additionalinfo, text) VALUES (214, null, 'Edinburg');
INSERT INTO public.option (id, additionalinfo, text) VALUES (215, null, 'Paříž');
INSERT INTO public.option (id, additionalinfo, text) VALUES (216, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (217, null, 'Varšava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (218, null, 'Glasgow');
INSERT INTO public.option (id, additionalinfo, text) VALUES (219, null, 'Leeds');
INSERT INTO public.option (id, additionalinfo, text) VALUES (220, null, 'Leeds');
INSERT INTO public.option (id, additionalinfo, text) VALUES (221, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (222, null, 'Paříž');
INSERT INTO public.option (id, additionalinfo, text) VALUES (223, null, 'Ostrava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (224, null, 'Praha');
INSERT INTO public.option (id, additionalinfo, text) VALUES (225, null, 'Praha');
INSERT INTO public.option (id, additionalinfo, text) VALUES (226, null, 'Bratislava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (227, null, 'Vídeň');
INSERT INTO public.option (id, additionalinfo, text) VALUES (228, null, 'Brno');
INSERT INTO public.option (id, additionalinfo, text) VALUES (229, null, 'Glasgow');
INSERT INTO public.option (id, additionalinfo, text) VALUES (230, null, 'Brno');
INSERT INTO public.option (id, additionalinfo, text) VALUES (231, null, 'Vídeň');
INSERT INTO public.option (id, additionalinfo, text) VALUES (232, null, 'Zlín');
INSERT INTO public.option (id, additionalinfo, text) VALUES (233, null, 'Vídeň');
INSERT INTO public.option (id, additionalinfo, text) VALUES (234, null, 'Bratislava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (235, null, 'Praha');
INSERT INTO public.option (id, additionalinfo, text) VALUES (236, null, 'Praha');
INSERT INTO public.option (id, additionalinfo, text) VALUES (237, null, 'Varšava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (238, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (239, null, 'Praha');
INSERT INTO public.option (id, additionalinfo, text) VALUES (240, null, 'Praha');
INSERT INTO public.option (id, additionalinfo, text) VALUES (241, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (242, null, 'Glasgow');
INSERT INTO public.option (id, additionalinfo, text) VALUES (243, null, 'Ostrava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (244, null, 'Vídeň');
INSERT INTO public.option (id, additionalinfo, text) VALUES (245, null, 'Leeds');
INSERT INTO public.option (id, additionalinfo, text) VALUES (246, null, 'Vídeň');
INSERT INTO public.option (id, additionalinfo, text) VALUES (247, null, 'Varšava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (248, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (249, null, 'Edinburg');
INSERT INTO public.option (id, additionalinfo, text) VALUES (250, null, 'Vídeň');
INSERT INTO public.option (id, additionalinfo, text) VALUES (251, null, 'Londýn');
INSERT INTO public.option (id, additionalinfo, text) VALUES (252, null, 'Ostrava');
INSERT INTO public.option (id, additionalinfo, text) VALUES (253, null, 'Edinburg');
INSERT INTO public.option (id, additionalinfo, text) VALUES (254, null, 'Vídeň');
SELECT setval('option_id_seq', 300);

--TESTPOSSIBLEANSWER
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (192, true, 236, 179);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (193, false, 223, 194);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (194, true, 195, 191);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (195, true, 199, 198);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (196, false, 254, 197);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (197, true, 246, 173);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (198, true, 240, 186);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (199, false, 244, 192);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (200, true, 210, 192);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (201, false, 230, 194);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (202, true, 208, 189);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (203, false, 197, 169);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (204, false, 218, 187);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (205, false, 250, 186);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (206, true, 194, 177);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (207, false, 253, 195);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (208, false, 206, 187);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (209, true, 198, 195);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (210, true, 205, 193);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (211, true, 228, 186);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (212, false, 222, 169);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (213, true, 227, 176);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (214, false, 231, 199);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (215, true, 203, 192);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (216, true, 209, 199);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (217, false, 202, 189);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (218, false, 193, 197);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (219, true, 239, 172);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (220, true, 224, 190);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (221, true, 235, 178);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (222, true, 212, 171);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (223, true, 220, 169);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (224, false, 196, 196);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (225, true, 234, 174);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (226, false, 232, 194);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (227, false, 229, 195);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (228, true, 221, 184);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (229, true, 226, 168);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (230, true, 213, 180);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (231, false, 215, 189);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (232, true, 252, 186);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (233, true, 192, 194);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (234, false, 245, 195);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (235, true, 243, 199);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (236, false, 217, 199);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (237, true, 204, 197);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (238, true, 225, 192);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (239, true, 233, 185);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (240, false, 249, 196);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (241, true, 211, 200);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (242, false, 237, 192);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (243, true, 201, 199);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (244, true, 251, 183);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (245, true, 238, 170);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (246, false, 200, 197);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (247, false, 247, 186);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (248, true, 241, 187);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (249, true, 216, 182);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (250, false, 242, 196);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (251, true, 219, 189);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (252, true, 248, 196);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (253, false, 214, 187);
INSERT INTO public.possibleanswer (id, correct, option, question) VALUES (254, true, 207, 169);
SELECT setval('possibleanswer_id_seq', 300);
-- TESTSUMMARY
INSERT INTO public.testsummary (id, amountofpoints, timefinished, timestarted, student, test) VALUES (121, 0, null, null, 4, 123);
INSERT INTO public.testsummary (id, amountofpoints, timefinished, timestarted, student, test) VALUES (122, 0, null, null, 2, 121);
INSERT INTO public.testsummary (id, amountofpoints, timefinished, timestarted, student, test) VALUES (123, 0, null, null, 3, 125);
INSERT INTO public.testsummary (id, amountofpoints, timefinished, timestarted, student, test) VALUES (124, 0, null, null, 5, 124);
INSERT INTO public.testsummary (id, amountofpoints, timefinished, timestarted, student, test) VALUES (125, 65, '2022-05-23 09:40:46.384000', '2022-05-23 09:39:01.141000', 1, 122);
SELECT setval('testsummary_id_seq', 160);

-- QUESTIONRESULTS
INSERT INTO public.questionresult (id, question, testsummary) VALUES (122, 192, 125);
INSERT INTO public.questionresult (id, question, testsummary) VALUES (123, 174, 125);
INSERT INTO public.questionresult (id, question, testsummary) VALUES (124, 179, 125);
INSERT INTO public.questionresult (id, question, testsummary) VALUES (125, 185, 125);
INSERT INTO public.questionresult (id, question, testsummary) VALUES (126, 200, 125);
INSERT INTO public.questionresult (id, question, testsummary) VALUES (127, 187, 125);
SELECT setval('questionresult_id_seq', 150);

--ANSWERS
INSERT INTO public.answers (resultid, possibleanswerid) VALUES (122, 200);
INSERT INTO public.answers (resultid, possibleanswerid) VALUES (122, 215);
INSERT INTO public.answers (resultid, possibleanswerid) VALUES (122, 238);
INSERT INTO public.answers (resultid, possibleanswerid) VALUES (123, 225);
INSERT INTO public.answers (resultid, possibleanswerid) VALUES (124, 192);
INSERT INTO public.answers (resultid, possibleanswerid) VALUES (125, 239);
INSERT INTO public.answers (resultid, possibleanswerid) VALUES (126, 241);
INSERT INTO public.answers (resultid, possibleanswerid) VALUES (127, 248);



