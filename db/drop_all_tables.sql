drop table family cascade;

drop table message cascade;

drop table user_account cascade;

drop table parent cascade;

drop table chat cascade;

drop table material cascade;

drop table summary cascade;

drop table scheduled_teaching cascade;

drop table attempt cascade;

drop table student cascade;

drop table task cascade;

drop table teaching cascade;

drop table teacher cascade;

drop table classroom cascade;

drop table subject cascade;
