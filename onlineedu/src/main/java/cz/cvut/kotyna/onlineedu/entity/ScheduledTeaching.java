/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.kotyna.onlineedu.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author daniel
 */
@Entity
@Table(name = "scheduled_teaching")
@NamedQueries({
    @NamedQuery(name = "ScheduledTeaching.findAll", query = "SELECT s FROM ScheduledTeaching s"),
    @NamedQuery(name = "ScheduledTeaching.findById", query = "SELECT s FROM ScheduledTeaching s WHERE s.id = :id"),
    @NamedQuery(name = "ScheduledTeaching.findByTimeFrom", query = "SELECT s FROM ScheduledTeaching s WHERE s.timeFrom = :timeFrom"),
    @NamedQuery(name = "ScheduledTeaching.findByTimeTo", query = "SELECT s FROM ScheduledTeaching s WHERE s.timeTo = :timeTo")})
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class ScheduledTeaching implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "time_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeFrom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "time_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeTo;
    @JoinColumn(name = "teaching", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Teaching teaching;

    public ScheduledTeaching() {
    }

    public ScheduledTeaching(Integer id) {
        this.id = id;
    }

    public ScheduledTeaching(Integer id, Date timeFrom, Date timeTo) {
        this.id = id;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTimeFrom() {
        return timeFrom;
    }

    public String getTimeFromFormated() {
        SimpleDateFormat nf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return nf.format(timeFrom);
    }

    public void setTimeFrom(Date timeFrom) {
        this.timeFrom = timeFrom;
    }

    public Date getTimeTo() {
        return timeTo;
    }

    public String getTimeToFormated() {
        SimpleDateFormat nf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return nf.format(timeTo);
    }

    public void setTimeTo(Date timeTo) {
        this.timeTo = timeTo;
    }

    public Teaching getTeaching() {
        return teaching;
    }

    public void setTeaching(Teaching teaching) {
        this.teaching = teaching;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScheduledTeaching)) {
            return false;
        }
        ScheduledTeaching other = (ScheduledTeaching) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.kotyna.onlineedu.entity.ScheduledTeaching[ id=" + id + " ]";
    }
    
}
