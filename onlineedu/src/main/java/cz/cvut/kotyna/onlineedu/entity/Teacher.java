/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.kotyna.onlineedu.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import cz.cvut.fel.tests.entity.Template.Template;
import cz.cvut.fel.tests.entity.Test.Exam;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author daniel
 */
@Entity
@Table(name = "teacher")
@NamedQueries({
        @NamedQuery(name = Teacher.FIND_ALL, query = "SELECT t FROM Teacher t"),
        @NamedQuery(name = "Teacher.findById", query = "SELECT t FROM Teacher t WHERE t.id = :id")})
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Teacher implements Serializable {

    public static final String FIND_ALL = "Teacher.findAll";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacher")
    private Collection<Teaching> teachingCollection;
    @JoinColumn(name = "classroom", referencedColumnName = "id")
    @OneToOne
    private Classroom classroom;
    @JoinColumn(name = "user_account", referencedColumnName = "id")
    @OneToOne(optional = false, cascade = CascadeType.ALL)
    private UserAccount userAccount;
    @OneToMany(mappedBy = "author")
    private List<Template> templateList;
    @OneToMany(mappedBy = "author")
    private List<Exam> examList;

    public List<Template> getTemplateList() {
        return templateList;
    }

    public List<Exam> getExamList() {
        return examList;
    }

    public Teacher() {
    }

    public Teacher(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<Teaching> getTeachingCollection() {
        return teachingCollection;
    }

    public void setTeachingCollection(Collection<Teaching> teachingCollection) {
        this.teachingCollection = teachingCollection;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Teacher)) {
            return false;
        }
        Teacher other = (Teacher) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.kotyna.onlineedu.entity.Teacher[ id=" + id + " ]";
    }

}
