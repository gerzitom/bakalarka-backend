package cz.cvut.kotyna.onlineedu;

import javax.annotation.security.DeclareRoles;
import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.authentication.mechanism.http.BasicAuthenticationMechanismDefinition;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 */
//@LoginConfig(authMethod = "MP-JWT")
@ApplicationPath("rest")
@BasicAuthenticationMechanismDefinition(realmName = "onlineEduRealm")
@ApplicationScoped
@DeclareRoles({"teacher", "student"})
public class JAXRSConfiguration extends Application {}
