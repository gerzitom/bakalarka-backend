/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.kotyna.onlineedu.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import cz.cvut.fel.tests.entity.UserInOrganization;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 *
 * @author daniel
 */
@Entity
@Table(name = "user_account")
@NamedQueries({
        @NamedQuery(name = UserAccount.FIND_ALL, query = "SELECT u FROM UserAccount u"),
        @NamedQuery(name = "UserAccount.findById", query = "SELECT u FROM UserAccount u WHERE u.id = :id"),
        @NamedQuery(name = "UserAccount.findByRole", query = "SELECT u FROM UserAccount u WHERE u.role = :role"),
        @NamedQuery(name = "UserAccount.findByUsername", query = "SELECT u FROM UserAccount u WHERE u.username = :username"),
        @NamedQuery(name = "UserAccount.findByPassword", query = "SELECT u FROM UserAccount u WHERE u.password = :password"),
        @NamedQuery(name = "UserAccount.findByEmail", query = "SELECT u FROM UserAccount u WHERE u.email = :email"),
        @NamedQuery(name = "UserAccount.findByFirstname", query = "SELECT u FROM UserAccount u WHERE u.firstname = :firstname"),
        @NamedQuery(name = "UserAccount.findBySurname", query = "SELECT u FROM UserAccount u WHERE u.surname = :surname"),
        @NamedQuery(name = "UserAccount.findByBirthday", query = "SELECT u FROM UserAccount u WHERE u.birthday = :birthday"),
        @NamedQuery(name = "UserAccount.findByRegistered", query = "SELECT u FROM UserAccount u WHERE u.registered = :registered"),
        @NamedQuery(name = "UserAccount.findByStreet", query = "SELECT u FROM UserAccount u WHERE u.street = :street"),
        @NamedQuery(name = "UserAccount.findByZip", query = "SELECT u FROM UserAccount u WHERE u.zip = :zip"),
        @NamedQuery(name = "UserAccount.findByPhone", query = "SELECT u FROM UserAccount u WHERE u.phone = :phone"),
        @NamedQuery(name = UserAccount.FIND_USER_ACCOUNT_BY_USERNAME, query = "select userAccount from UserAccount userAccount where userAccount.username = :username")})
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserAccount implements Serializable {

    public static final String FIND_ALL = "UserAccount.findAll";
    public static final String FIND_USER_ACCOUNT_BY_USERNAME = "UserAccount.findUserAccountByUsername";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "role")
    private String role;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "password")
    private String password;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "firstname")
    private String firstname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "surname")
    private String surname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "gender")
    private String gender;
    @Column(name = "birthday")
    private Date birthday;
    @Column(name = "registered")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registered;
    @Size(max = 128)
    @Column(name = "street")
    private String street;
    @Size(max = 32)
    @Column(name = "zip")
    private String zip;
    @Size(max = 128)
    @Column(name = "city")
    private String city;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 64)
    @Column(name = "phone")
    private String phone;

    @OneToMany(mappedBy = "user_account")
    private List<UserInOrganization> userInOrganizations;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userAccount")
    private Collection<Message> messageCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userAccount")
    private Parent parent;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userAccount")
    private Student student;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userAccount")
    private Teacher teacher;
    @OneToMany(mappedBy = "userAccount", cascade = CascadeType.ALL)
    private Collection<UsersAvatar> usersAvatars;

    public UserAccount() {
    }

    public UserAccount(Integer id) {
        this.id = id;
    }

    public UserAccount(Integer id, String role, String username, String password, String email, String firstname, String surname) {
        this.id = id;
        this.role = role;
        this.username = username;
        this.password = password;
        this.email = email;
        this.firstname = firstname;
        this.surname = surname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAge() {
        if (birthday == null) return null;
        LocalDate localDateBirthday = LocalDate.ofInstant(birthday.toInstant(), ZoneId.systemDefault());
        return Period.between(localDateBirthday, LocalDate.now()).getYears();
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getBirthdayFormatted() {
        SimpleDateFormat nf = new SimpleDateFormat("dd. MM. yyyy");
        if (birthday != null) {
            return nf.format(birthday);
        }
        else {
            return "";
        }
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getRegistered() {
        return registered;
    }

    public String getRegisteredFormatted() {
        SimpleDateFormat nf = new SimpleDateFormat("dd. MM. yyyy HH:mm");
        if (registered != null) {
            return nf.format(registered);
        }
        else {
            return "";
        }
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @XmlTransient
    public Collection<Message> getMessageCollection() {
        return messageCollection;
    }

    public void setMessageCollection(Collection<Message> messageCollection) {
        this.messageCollection = messageCollection;
    }

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public String getGenderFormatted() {
        switch (gender) {
            case "male" : return "Muž";
            case "female" : return "Žena";
            default : return "";
        }
    }

    public List<UserInOrganization> getUserInOrganizations() {
        if(userInOrganizations == null) return new ArrayList<>();
        return userInOrganizations;
    }

    public void setUserInOrganizations(List<UserInOrganization> userInOrganizations) {
        this.userInOrganizations = userInOrganizations;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRoleFormated() {
        switch (role) {
            case "student" : return "Student";
            case "teacher" : return "Vyučující";
            case "parent" : return "Rodič";
            case "admin" : return "Administrátor";
            default : return "Neznámá role";
        }
    }

    public String getFullAddress() {
        if (street == null && zip == null && city == null) return "";
        else if (street != null && zip != null && city != null) return street + ((!street.isEmpty() && !zip.isEmpty()) || (!street.isEmpty() && !city.isEmpty())? ", " : "") + zip + (!city.isEmpty() ? " " : "") + city;
        else if (street != null && zip != null) return street + (!street.isEmpty() && !zip.isEmpty() ? ", " : "") + zip;
        else if (street != null && city != null) return street + (!street.isEmpty() && !city.isEmpty() ? ", " : "") + city;
        else if (street != null) return street;
        else return zip;
    }

    public String getFullName() {
        return this.firstname + " " + this.surname;
    }

    public Collection<UsersAvatar> getUsersAvatars() {
        return usersAvatars;
    }

    public void setUsersAvatars(Collection<UsersAvatar> usersAvatars) {
        this.usersAvatars = usersAvatars;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccount)) {
            return false;
        }
        UserAccount other = (UserAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.kotyna.onlineedu.entity.UserAccount[ id=" + id + " ]";
    }

}
