package cz.cvut.fel.tests.dao.Test;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Test.PossibleAnswer;

import javax.ejb.Stateless;

@Stateless
public class PossibleAnswerDao extends GenericDao<PossibleAnswer> {
    public PossibleAnswerDao(){
        super(PossibleAnswer.class);
    }
}
