package cz.cvut.fel.tests.dto.Websocket;

import javax.json.bind.JsonbBuilder;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

public class TestCompleteInformationDecoder implements Decoder.Text<TestCompleteInformation> {
    @Override
    public TestCompleteInformation decode(String s) throws DecodeException {
        //Using JSON-B (JSR 367) API for mapping from JSON to T
        return JsonbBuilder.create().fromJson(s, TestCompleteInformation.class);
    }

    @Override
    public boolean willDecode(String s) {
        return true;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
