package cz.cvut.fel.tests.controller;

import cz.cvut.fel.tests.dto.*;
import cz.cvut.fel.tests.dto.CorrectAndUserAnswers.TestAnswers;
import cz.cvut.fel.tests.dto.CorrectAndUserAnswers.UserAnswersDto;
import cz.cvut.fel.tests.dto.OpenTextAnswer.TextAnswerEvaluateDto;
import cz.cvut.fel.tests.dto.OpenTextAnswer.TextAnswerReadDto;
import cz.cvut.fel.tests.dto.OpenTextAnswer.TextAnswerSaveDto;
import cz.cvut.fel.tests.dto.Test.*;
import cz.cvut.fel.tests.dto.TestSummary.TestSummaryReadDto;
import cz.cvut.fel.tests.entity.Test.Test;
import cz.cvut.fel.tests.fasade.TestFacade;
import cz.cvut.fel.tests.service.Test.TestService;
import cz.cvut.fel.tests.service.TextAnswerService;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RequestScoped
@Path("/tests")
public class TestController {
    @EJB
    private TestFacade testFacade;

    @EJB
    private TestService testService;

    @EJB
    private TextAnswerService textAnswerService;


    @GET
    public Response getAllTests(){
        return Response.ok(testService.getAllTests().stream().map(TestReadDto::new)
                .collect(Collectors.toList())).build();
    }

    @GET
    @Path("/{testId}")
    public Response getTest(
            @PathParam("testId") Long testId
    ){
        return Response.ok(testService.getTest(testId)).build();
    }

    @DELETE
    @Path("/{testId}")
    public Response deleteTest(
            @PathParam("testId") Long testId
    ){
        return Response.ok(testService.deleteTest(testId)).build();
    }


    @POST
    @Path("/{templateId}/generateTest")
    public Response generateTest(
            @RequestBody ExamSaveDto dto,
            @PathParam("templateId") Long templateId
    ){
        List<Test> tests = testFacade.generateTest(dto, templateId);
        List<TestReadDto> testReadDtos = tests.stream().map(TestReadDto::new).collect(Collectors.toList());
        return Response.ok(testReadDtos).build();
    }

    @GET
    @Path("/{testId}/questionGroups")
    public Response getAllQuestions(
            @PathParam("testId") Long testId
    ){
        return Response.ok(testFacade.getQuestionGroups(testId)
                .stream().map(QuestionGroupReadDto::new)
                .collect(Collectors.toList())).build();
    }

    @GET
    @Path("/{testId}/testparts")
    public Response getAllTestParts (
            @PathParam("testId") Long testId
    ){
        return Response.ok(testService.getTestParts(testId)
                .stream().map(TestPartReadDto::new)
                .collect(Collectors.toList())).build();
    }

    @POST
    @Path("{testId}/saveAnswer")
    public void saveAnswer(
            @PathParam("testId") Long testId,
            @RequestBody AnswerSaveDto answerSaveDto
    ){
        testService.saveAnswerToQuestionResult(testId, answerSaveDto);
    }

    @POST
    @Path("{testId}/saveOpenAnswer")
    public TextAnswerReadDto saveOpenAnswer(
            @PathParam("testId") Long testId,
            @RequestBody TextAnswerSaveDto answerSaveDto
    ){
        return new TextAnswerReadDto(testService.saveAnswerToOpenQuestion(testId, answerSaveDto));
    }

    @GET
    @Path("/TextAnswer/{id}")
    public TextAnswerReadDto getOpenAnswer(
            @PathParam("id") Long id
    ){
        return new TextAnswerReadDto(textAnswerService.getAnswer(id));
    }

    @GET
    @Path("question/{id}/openAnswer")
    public List<TextAnswerReadDto> getOpenAnswerFromQuestion(
            @PathParam("id") Long id
    ) {
        return textAnswerService.getAnswerFromQuestion(id).stream()
                .map(TextAnswerReadDto::new).collect(Collectors.toList());
    }
    @GET
    @Path("{testId}/TextAnswers")
    public Response getAllOpenAnswers(
            @PathParam("testId") Long id
    ){
        return Response.ok(testService.getAllTextAnswers(id).stream()
                .map(TextAnswerReadDto::new)
                .collect(Collectors.toList())).build();
    }

    //Evaluate open text answer, done manually by a teacher
    @POST
    @Path("{answerId}/evaluate")
    public TextAnswerReadDto evaluateOpenAnswer(
            @PathParam("answerId") Long id,
            @RequestBody  TextAnswerEvaluateDto dto
    ){
        return textAnswerService.evaluateTextAnswer(id, dto);
    }

    @GET
    @Path("{testId}/summary")
    public Response getTestSummary(
            @PathParam("testId") Long id
    ){
        return Response.ok(testService.getSubmittedTests(id)
                .stream().map(TestSummaryReadDto::new)
                .collect(Collectors.toList())).build();
    }


//    @GET
//    @Path("{testId}/evaluateTest")
//    public TestSummaryReadDto evaluateTest(
//            @PathParam("testId") Long testId
//    ){
//        return testService.evaluateTest(testId);
//    }

    @POST
    @Path("{testId}/start")
    public Response startTest(
            @PathParam("testId") Long testId
    ){
        return Response.ok(new TestReadDto(testService
                .startTest(testId))).build();
    }

    @POST
    @Path("/{testId}/end")
    public Response finishAndEvaluateTest(
            @PathParam("testId") Long testId
    ){
        return Response.ok(new TestAnswers(testService.evaluateTest(testId))).build();
    }

    @GET
    @Path("{testId}/answers")
    public TestAnswers getTestAnswers(
            @PathParam("testId") Long id
    ){
        return testService.getTestAnswers(id);
    }

    @GET
    @Path("/{testId}/userAnswers")
    public List<UserAnswersDto> getUserAnswers(
            @PathParam("testId") Long id
    ){
        return testService.getUserAnswers(id)
                .stream().map(UserAnswersDto::new)
                .collect(Collectors.toList());
    }


}
