package cz.cvut.fel.tests.dao.Test;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Test.Option;

import javax.ejb.Stateless;

@Stateless
public class OptionDao extends GenericDao<Option> {
    public OptionDao(){
        super(Option.class);
    }
}
