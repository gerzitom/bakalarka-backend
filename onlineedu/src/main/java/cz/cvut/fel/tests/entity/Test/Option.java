package cz.cvut.fel.tests.entity.Test;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Option {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String text;
    private String additionalInfo;

    @OneToMany(mappedBy = "option", cascade = CascadeType.PERSIST)
    private List<PossibleAnswer> possibleAnswers;

    public Option(Long id, String additionalInfo, String text) {
        this.id = id;
        this.additionalInfo = additionalInfo;
        this.text = text;
    }
}
