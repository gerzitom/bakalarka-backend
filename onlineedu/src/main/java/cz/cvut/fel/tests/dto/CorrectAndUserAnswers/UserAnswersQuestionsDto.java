package cz.cvut.fel.tests.dto.CorrectAndUserAnswers;

import cz.cvut.fel.tests.dto.Test.PossibleAnswerReadDto;
import cz.cvut.fel.tests.entity.Test.PossibleAnswer;
import cz.cvut.fel.tests.entity.Test.Question;
import cz.cvut.fel.tests.entity.Test.QuestionResult;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class UserAnswersQuestionsDto {
    private Long questionId;
    private List<PossibleAnswerReadDto> possibleAnswers;

    public UserAnswersQuestionsDto(Question entity){
        this.questionId = entity.getId();
        this.possibleAnswers = getUserAnswers(entity).stream().map(PossibleAnswerReadDto::new).collect(Collectors.toList());
    }

    private List<PossibleAnswer> getUserAnswers(Question entity){
        List<QuestionResult> questionResults = new ArrayList<>(entity.getQuestionResults());
        List<PossibleAnswer> possibleAnswers = new ArrayList<>();
        for(QuestionResult q : questionResults){
            possibleAnswers.addAll(q.getAnswers());
        }
        return possibleAnswers;
    }
}
