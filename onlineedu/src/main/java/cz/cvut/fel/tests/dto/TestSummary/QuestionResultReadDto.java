package cz.cvut.fel.tests.dto.TestSummary;

import cz.cvut.fel.tests.dto.Test.QuestionReadDto;
import cz.cvut.fel.tests.entity.Test.QuestionResult;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class QuestionResultReadDto {
    private Long id;
    private QuestionReadDto questionReadDto;
    private List<PossibleAnswerReadDtoSummary> possibleAnswerReadDtoSummaryList;

    public QuestionResultReadDto(QuestionResult entity){
        this.id = entity.getId();
        this.questionReadDto  = new QuestionReadDto(entity.getQuestion());
    }
}
