package cz.cvut.fel.tests.dao;

import cz.cvut.kotyna.onlineedu.entity.Classroom;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class ClassRoomDao extends GenericDao<Classroom> {
    @PersistenceContext
    EntityManager em;

    public ClassRoomDao(){
        super(Classroom.class);
    }

    public List<Classroom> getAllClassRooms(){
        return em.createNamedQuery(Classroom.FIND_ALL, Classroom.class).getResultList();
    }

}
