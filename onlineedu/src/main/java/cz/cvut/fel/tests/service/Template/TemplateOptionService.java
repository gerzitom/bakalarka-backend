package cz.cvut.fel.tests.service.Template;

import cz.cvut.fel.tests.dao.Template.TemplatePossibleAnswerDao;
import cz.cvut.fel.tests.dao.Template.TemplateQuestionDao;
import cz.cvut.fel.tests.dao.Template.SingleOptionDao;
import cz.cvut.fel.tests.dto.Template.Option.TemplateOptionSaveDto;
import cz.cvut.fel.tests.entity.Template.TemplatePossibleAnswer;
import cz.cvut.fel.tests.entity.Template.TemplateOption;
import cz.cvut.fel.tests.exceptions.BasicException;

import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless
public class TemplateOptionService {
    @EJB
    private TemplateQuestionDao templateQuestionDao;
    @EJB
    private SingleOptionDao singleOptionDao;
    @EJB
    private TemplatePossibleAnswerDao templatePossibleAnswerDao;

    public TemplateOption createSingleOptionForQuestion(Long questionId, TemplateOptionSaveDto dto){
        TemplateOption newTemplateOption = toEntityFromDto(dto);
        TemplatePossibleAnswer pa = new TemplatePossibleAnswer();
        pa.setOption(newTemplateOption);
        pa.setTemplateQuestion(templateQuestionDao.find(questionId));
        pa.setCorrect(dto.isCorrect());
        singleOptionDao.save(newTemplateOption);
        templatePossibleAnswerDao.save(pa);
        return newTemplateOption;
    }

    public TemplatePossibleAnswer updatePossibleAnswer(Long optionId, TemplateOptionSaveDto dto){
        TemplatePossibleAnswer templatePossibleAnswer = templatePossibleAnswerDao.find(optionId);
        templatePossibleAnswer.setCorrect(dto.isCorrect());
        TemplateOption templateOption = templatePossibleAnswer.getOption();
        if(dto.getText() != null) templateOption.setText(dto.getText());
        singleOptionDao.update(templateOption);
        templatePossibleAnswerDao.update(templatePossibleAnswer);
        return templatePossibleAnswer;
    }

    public TemplateOption toEntityFromDto(TemplateOptionSaveDto dto){
        TemplateOption newTemplateOption = new TemplateOption();
        newTemplateOption.setText(dto.getText());
        newTemplateOption.setAdditionalInfo(dto.getAdditionalInfo());
        return newTemplateOption;
    }

    public void deleteOption(Long optionId) {
        if(singleOptionDao.find(optionId) == null){
            throw new BasicException("Moznost nebyla nalezena");
        }
        singleOptionDao.delete(optionId);
    }
}
