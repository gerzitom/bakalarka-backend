package cz.cvut.fel.tests.dto.CorrectAndUserAnswers;

import cz.cvut.fel.tests.dto.Test.PossibleAnswerReadDto;
import cz.cvut.fel.tests.entity.Test.PossibleAnswer;
import cz.cvut.fel.tests.entity.Test.Question;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class RightAnswersQuestionsDto {
    private Long questionId;
    private List<PossibleAnswerReadDto> possibleAnswers;

    public RightAnswersQuestionsDto(Question entity){
        this.questionId = entity.getId();
        this.possibleAnswers = getRightAnswers(entity).stream().map(PossibleAnswerReadDto::new).collect(Collectors.toList());
    }

    public List<PossibleAnswer> getRightAnswers(Question entity){
       List<PossibleAnswer> allPossibleAnswers = entity.getPossibleAnswers();
       List<PossibleAnswer> rightAnswers = new ArrayList<>();
       for(PossibleAnswer p : allPossibleAnswers){
           if (p.getCorrect()){
               rightAnswers.add(p);
           }
       }
       return rightAnswers;
    }

}
