package cz.cvut.fel.tests.dto.OpenTextAnswer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TextAnswerEvaluateDto {
    private Double points;
}
