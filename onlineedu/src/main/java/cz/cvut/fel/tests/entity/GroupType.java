package cz.cvut.fel.tests.entity;

public enum GroupType {
    SIMPLE, SPOJOVACKA, FILL_GAPS, OPEN
}
