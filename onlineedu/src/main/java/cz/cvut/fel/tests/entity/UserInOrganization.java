package cz.cvut.fel.tests.entity;

import cz.cvut.kotyna.onlineedu.entity.UserAccount;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class UserInOrganization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "organization", referencedColumnName = "id")
    private Organization organization;

    @ManyToOne
    @JoinColumn(name = "user_account", referencedColumnName = "id")
    private UserAccount user_account;

}
