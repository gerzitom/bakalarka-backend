package cz.cvut.fel.tests.entity.Test;

import cz.cvut.fel.tests.entity.Template.TextAnswer;
import cz.cvut.fel.tests.entity.options.AnswerType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 4000)
    private String content;

    @Enumerated(EnumType.STRING)
    private AnswerType answerType;

    private Double points;

    @ManyToOne
    @JoinColumn(name = "questionGroup", referencedColumnName = "id")
    private QuestionGroup questionGroup;

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PossibleAnswer> possibleAnswers;

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    private List<QuestionResult> questionResults;

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    private List<TextAnswer> textAnswers;

    public Question(Long id, String content, AnswerType answerType, Double points) {
        this.id = id;
        this.content = content;
        this.answerType = answerType;
        this.points = points;
    }
}
