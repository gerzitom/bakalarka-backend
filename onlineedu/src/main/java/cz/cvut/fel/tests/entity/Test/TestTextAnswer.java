package cz.cvut.fel.tests.entity.Test;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.awt.*;

@Entity
@Getter
@Setter
public class TestTextAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private TextArea text;
    private Boolean marked;
    private Double pointsReceived;

    @ManyToOne
    @JoinColumn(name = "question", referencedColumnName = "id")
    private Question question;

}
