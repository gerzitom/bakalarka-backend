package cz.cvut.fel.tests.dao.Template;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Template.TemplatePart;

import javax.ejb.Stateless;

@Stateless
public class TemplatePartDao extends GenericDao<TemplatePart> {
    public TemplatePartDao(){
        super(TemplatePart.class);
    }
}
