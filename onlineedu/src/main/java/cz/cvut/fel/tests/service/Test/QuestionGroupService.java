package cz.cvut.fel.tests.service.Test;

import cz.cvut.fel.tests.dao.Test.QuestionGroupDao;
import cz.cvut.fel.tests.entity.Template.TemplateQuestion;
import cz.cvut.fel.tests.entity.Template.TemplateQuestionGroup;
import cz.cvut.fel.tests.entity.Test.Question;
import cz.cvut.fel.tests.entity.Test.QuestionGroup;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class QuestionGroupService {
    @EJB
    private QuestionService questionService;
    @EJB
    private QuestionGroupDao questionGroupDao;

    public QuestionGroup generateQuestionGroup(TemplateQuestionGroup templateQuestionGroup){
        QuestionGroup questionGroup = fromQuestionGroupToGeneratedQuestionGroup(templateQuestionGroup);
        List<TemplateQuestion> templateQuestions = templateQuestionGroup.getTemplateQuestions();
        for(TemplateQuestion q : templateQuestions){
            questionService.generateTestQuestion(q, questionGroup);
        }
        return questionGroup;
    }

    public QuestionGroup fromQuestionGroupToGeneratedQuestionGroup(TemplateQuestionGroup templateQuestionGroup){
        QuestionGroup questionGroup = new QuestionGroup(templateQuestionGroup.getInstructions(), templateQuestionGroup.getPoints(), templateQuestionGroup.getGroupType());
        questionGroupDao.save(questionGroup);
        return questionGroup;
    }
}
