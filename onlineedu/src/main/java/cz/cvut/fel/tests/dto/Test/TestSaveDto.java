package cz.cvut.fel.tests.dto.Test;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestSaveDto {
    private String name;
    private int duration;
    private Integer classroomId;
    private Date startDate;
}
