package cz.cvut.fel.tests.dao;

import cz.cvut.kotyna.onlineedu.entity.Student;

import javax.ejb.Stateless;

@Stateless
public class StudentDao extends GenericDao<Student>{
    public StudentDao(){
        super(Student.class);
    }
}
