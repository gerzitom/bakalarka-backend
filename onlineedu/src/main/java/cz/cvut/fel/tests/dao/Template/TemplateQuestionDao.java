package cz.cvut.fel.tests.dao.Template;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Template.TemplateQuestion;


import javax.ejb.Stateless;


@Stateless
public class TemplateQuestionDao extends GenericDao<TemplateQuestion> {
    public TemplateQuestionDao() {
        super(TemplateQuestion.class);
    }
}
