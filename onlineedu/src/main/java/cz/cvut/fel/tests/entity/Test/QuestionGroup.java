package cz.cvut.fel.tests.entity.Test;

import cz.cvut.fel.tests.entity.GroupType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QuestionGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String instructions;
    private int points;
    @Enumerated(EnumType.STRING)
    private GroupType groupType;

    @OneToMany(mappedBy = "questionGroup", cascade = CascadeType.ALL)
    private List<Question> questions;

    @OneToMany(mappedBy = "questionGroup")
    private List<TestPart> testParts;

    public QuestionGroup(String instructions, int points, GroupType groupType) {
        this.instructions = instructions;
        this.points = points;
        this.groupType = groupType;
    }
}
