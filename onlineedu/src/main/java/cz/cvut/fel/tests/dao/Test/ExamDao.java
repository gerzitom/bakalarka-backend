package cz.cvut.fel.tests.dao.Test;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Test.Exam;

import javax.ejb.Stateless;

@Stateless
public class ExamDao extends GenericDao<Exam> {
    public ExamDao(){
        super(Exam.class);
    }
}