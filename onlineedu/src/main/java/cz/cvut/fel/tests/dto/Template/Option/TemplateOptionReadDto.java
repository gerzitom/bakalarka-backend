package cz.cvut.fel.tests.dto.Template.Option;

import cz.cvut.fel.tests.entity.Template.TemplateOption;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TemplateOptionReadDto {
    private Long id;
    private String text;
    private String additionalInfo;

    public TemplateOptionReadDto(TemplateOption entity) {
        this.id = entity.getId();
        this.text = entity.getText();
        this.additionalInfo = entity.getAdditionalInfo();
    }
}
