package cz.cvut.fel.tests.entity.options;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.tests.entity.QuestionAnswers;
import lombok.SneakyThrows;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class AnswersConverter implements AttributeConverter<QuestionAnswers, String> {
    ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public String convertToDatabaseColumn(QuestionAnswers questionAnswers) {
        try {
            return objectMapper.writeValueAsString(questionAnswers);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    @SneakyThrows
    @Override
    public QuestionAnswers convertToEntityAttribute(String s) {
        if(s == null) return null;
        return objectMapper.readValue(s, QuestionAnswers.class);
    }
}
