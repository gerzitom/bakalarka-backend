package cz.cvut.fel.tests.dto.Template.Option;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TemplateOptionSaveDto {
    private String text;
    private String additionalInfo;
    private boolean correct;
}
