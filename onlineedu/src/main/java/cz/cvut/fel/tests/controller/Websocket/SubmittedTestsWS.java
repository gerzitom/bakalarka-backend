package cz.cvut.fel.tests.controller.Websocket;

import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

@ServerEndpoint(
        value = "/finishTest/{username}",
        encoders = {MessageEncoder.class},
        decoders = {MessageDecoder.class}
)
public class SubmittedTestsWS {
    @Inject
    ChatSessionController chatSessionController;
    private static Session session;
    private static Set<Session> chatters = new CopyOnWriteArraySet<>();

    @OnOpen
    public void messageOpen(Session session,
                            @PathParam("username") String username) throws IOException,
            EncodeException {
        this.session = session;
        Map<String,String> chatusers = chatSessionController.getUsers();
        chatusers.put(session.getId(), username);
        chatSessionController.setUsers(chatusers);
        chatters.add(session);
        Message message = new Message();
        message.setUsername(username);
        message.setMessage("Welcome " + username);
        broadcast(message);
    }

    @OnMessage
    public void messageReceiver(Session session, Message message) throws IOException, EncodeException {
        Map<String,String> chatusers = chatSessionController.getUsers();
        message.setUsername(chatusers.get(session.getId()));
        broadcast(message);
    }

    public void testSubmitted(Long examId) {
        broadcast(new Message(examId.toString(), "submitted"));
    }

    private static void broadcast(Message message) {

        try{
            chatters.forEach(session -> {
                synchronized (session) {
                    try {
                        session.getBasicRemote().
                                sendObject(message);
                    } catch (IOException | EncodeException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e){

        }
    }
}
