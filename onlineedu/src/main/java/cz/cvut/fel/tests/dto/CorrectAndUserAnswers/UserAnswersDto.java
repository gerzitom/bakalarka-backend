package cz.cvut.fel.tests.dto.CorrectAndUserAnswers;

import cz.cvut.fel.tests.entity.Test.QuestionGroup;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class UserAnswersDto {
    private Long questionGroupId;
    private List<UserAnswersQuestionsDto> userAnswersQuestionsDtos;

    public UserAnswersDto(QuestionGroup entity) {
        this.questionGroupId = entity.getId();
        this.userAnswersQuestionsDtos = entity.getQuestions().stream().map(UserAnswersQuestionsDto::new).collect(Collectors.toList());
    }
}


