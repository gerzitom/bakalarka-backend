package cz.cvut.fel.tests.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AnswerSaveDto {
    private Long questionId;
    private List<Long> answerIds;

    public AnswerSaveDto(Long questionId, List<Long> answerIds) {
        this.questionId = questionId;
        this.answerIds = answerIds;
    }
}
