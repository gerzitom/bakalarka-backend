package cz.cvut.fel.tests.controller;

import cz.cvut.fel.tests.dto.user.UserLoginDto;
import cz.cvut.fel.tests.dto.user.UserReadDto;
import cz.cvut.fel.tests.dto.user.UserRegisterDto;
import cz.cvut.fel.tests.exceptions.BasicException;
import cz.cvut.fel.tests.exceptions.ThrowException;
import cz.cvut.kotyna.onlineedu.entity.UserAccount;
import cz.cvut.kotyna.onlineedu.service.AuthService;
import cz.cvut.kotyna.onlineedu.service.UserService;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

@RequestScoped
@ThrowException
@Path("/user")
public class UserController {

    @EJB
    private UserService userService;

    @POST
    @Path("/register")
    @PermitAll
    public Response registerUser(UserRegisterDto newUserDto) throws UnsupportedEncodingException, NoSuchAlgorithmException, BasicException {
        approveNewAccount(newUserDto);
        UserAccount newUser = createNewUser(newUserDto);
        userService.saveUserAccount(newUser);
        return Response
                .ok(new UserReadDto(newUser))
                .build();
    }

    @POST
    @Path("/login")
    @PermitAll
    public Response login(UserLoginDto loginDto){
        return Response.ok("OK").build();
    }

    @GET
    @Path("/me")
    @RolesAllowed({"student", "teacher"})
    public Response getUserInformation() {
        UserAccount user = userService.getLoggedInUser();
        return Response
                .ok(new UserReadDto(user))
                .build();
    }

    private void approveNewAccount(UserRegisterDto newUserDto) throws BasicException {
        UserAccount foundUser = userService.findByUsername(newUserDto.getUsername());
        if(foundUser != null) throw new BasicException("Username already used");
    }

    private UserAccount createNewUser(UserRegisterDto newUserDto) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        UserAccount newUser = new UserAccount();
        newUser.setRole("admin");
        newUser.setRegistered(new Date());
        newUser = userService.generateUserAccountUsernameAndPassword(newUser);
        newUser.setPassword(AuthService.encodeSHA256(newUserDto.getPassword(), ""));
        newUser.setUsername(newUserDto.getUsername());
        newUser.setGender("male");
        newUser.setEmail(newUserDto.getEmail());
        newUser.setFirstname(newUserDto.getFirstName());
        newUser.setSurname(newUserDto.getSurname());
        return newUser;
    }
}
