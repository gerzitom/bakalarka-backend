package cz.cvut.fel.tests.security;

import cz.cvut.kotyna.onlineedu.entity.UserAccount;
import cz.cvut.kotyna.onlineedu.service.AuthService;
import cz.cvut.kotyna.onlineedu.service.UserService;
import static javax.security.enterprise.identitystore.CredentialValidationResult.INVALID_RESULT;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.Password;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import javax.xml.registry.infomodel.User;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@ApplicationScoped
public class MyIdentityStore implements IdentityStore {

    @EJB
    private UserService userService;

    @Override
    public CredentialValidationResult validate(Credential credential) {
        if (credential instanceof UsernamePasswordCredential) {
            UsernamePasswordCredential userCredential = UsernamePasswordCredential
                    .class.cast(credential);

            final String userPassword = userCredential.getPasswordAsString();
            final String username = userCredential.getCaller();

            UserAccount foundUser = userService.findByUsername(username);
            if(foundUser == null) return INVALID_RESULT;

            boolean rightPassword = checkPassword(foundUser, userPassword);
            if(rightPassword){
                return new CredentialValidationResult(foundUser.getUsername(), new HashSet<>(Collections.singleton(foundUser.getRole())));
            }
        }
        return INVALID_RESULT;
    }

    private boolean checkPassword(UserAccount userAccount, String passwordToCheck){
        try {
            String hashedPassword = AuthService.encodeSHA256(passwordToCheck, "");
            return userAccount.getPassword().equals(hashedPassword);
        } catch (Exception e) {
            return false;
        }
    }


    @Override
    public int priority() {
        return 10;
    }
}
