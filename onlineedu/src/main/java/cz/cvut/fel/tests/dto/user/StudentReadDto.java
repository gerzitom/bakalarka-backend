package cz.cvut.fel.tests.dto.user;

import cz.cvut.fel.tests.dto.ClassroomReadDto;
import cz.cvut.kotyna.onlineedu.entity.Student;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentReadDto {
    private Integer classroomId;
    private String classroomName;
    public StudentReadDto(Student student) {
        this.classroomId = student.getClassroom().getId();
        this.classroomName = student.getClassroom().getName();
    }
}
