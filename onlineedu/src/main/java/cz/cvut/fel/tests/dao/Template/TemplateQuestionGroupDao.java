package cz.cvut.fel.tests.dao.Template;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Template.TemplateQuestionGroup;

import javax.ejb.Stateless;

@Stateless
public class TemplateQuestionGroupDao extends GenericDao<TemplateQuestionGroup> {
    public TemplateQuestionGroupDao(){
        super(TemplateQuestionGroup.class);
    }
}
