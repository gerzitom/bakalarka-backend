package cz.cvut.fel.tests.exceptions;

import cz.cvut.fel.tests.dto.ErrorMessageDto;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MyExceptionMapper implements ExceptionMapper<Throwable> {
    @Override
    public Response toResponse(Throwable throwable) {
        String message = throwable.getMessage();
        return Response
                .status(400)
                .entity(new ErrorMessageDto(message))
                .build();
    }
}
