package cz.cvut.fel.tests.dto.Template.Question;

import cz.cvut.fel.tests.dto.Template.PossibleAnswer.TemplatePossibleAnswerReadDto;
import cz.cvut.fel.tests.entity.Template.TemplateQuestion;
import cz.cvut.fel.tests.entity.TestTemplatePreferredType;
import cz.cvut.fel.tests.entity.options.AnswerType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TemplateQuestionReadDto {
    private Long id;
    private String content;
    private AnswerType answerType;
    private List<TemplatePossibleAnswerReadDto> possibleAnswers;
    private Double points;

    public TemplateQuestionReadDto(TemplateQuestion entity) {
        this.id = entity.getId();
        this.content = entity.getContent();
        this.answerType = entity.getAnswerType();
        this.possibleAnswers = entity.getTemplatePossibleAnswers().stream().map(TemplatePossibleAnswerReadDto::new).collect(Collectors.toList());
        this.points = entity.getPoints();
    }
}
