package cz.cvut.fel.tests.entity.Test;

import cz.cvut.kotyna.onlineedu.entity.Student;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class TestSummary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date timeStarted;
    private Date timeFinished;
    private double amountOfPoints;

    @ManyToOne
    @JoinColumn(name = "test", referencedColumnName = "id")
    private Test test;

    @ManyToOne
    @JoinColumn(name = "student", referencedColumnName = "id")
    private Student student;

    @OneToMany(mappedBy = "testSummary", cascade = CascadeType.REMOVE)
    private List<QuestionResult> questionResults;




}
