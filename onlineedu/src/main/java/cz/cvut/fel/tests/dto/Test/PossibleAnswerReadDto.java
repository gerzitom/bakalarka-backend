package cz.cvut.fel.tests.dto.Test;

import cz.cvut.fel.tests.entity.Test.PossibleAnswer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PossibleAnswerReadDto {
    private Long id;
    private OptionReadDto singleOption;

    public PossibleAnswerReadDto(PossibleAnswer entity){
        this.id = entity.getId();
        this.singleOption = new OptionReadDto(entity.getOption());
    }
}
