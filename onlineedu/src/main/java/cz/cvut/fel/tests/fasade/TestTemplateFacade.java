package cz.cvut.fel.tests.fasade;

import cz.cvut.fel.tests.dao.Template.TemplateQuestionGroupDao;
import cz.cvut.fel.tests.dto.Template.Option.TemplateOptionSaveDto;
import cz.cvut.fel.tests.dto.Template.Question.TemplateQuestionSaveDto;
import cz.cvut.fel.tests.dto.Template.Part.TemplatePartSaveDto;
import cz.cvut.fel.tests.dto.Template.TemplateSaveDto;
import cz.cvut.fel.tests.dto.Template.QuestionGroup.TemplateQuestionGroupSaveDto;
import cz.cvut.fel.tests.entity.Template.*;
import cz.cvut.fel.tests.service.Template.TemplateQuestionGroupService;
import cz.cvut.fel.tests.service.Template.TemplateQuestionService;
import cz.cvut.fel.tests.service.Template.TemplateOptionService;
import cz.cvut.fel.tests.service.Template.TemplatePartService;
import cz.cvut.fel.tests.service.Template.TemplateService;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class TestTemplateFacade {

    @EJB
    private TemplateService templateService;

    @EJB
    private TemplatePartService templatePartService;

    @EJB
    private TemplateQuestionGroupService templateQuestionGroupService;

    @EJB
    private TemplateQuestionGroupDao templateQuestionGroupDao;

    @EJB
    private TemplateQuestionService templateQuestionService;

    @EJB
    private TemplateOptionService templateOptionService;


    public Template saveTestTemplate(TemplateSaveDto dto){
        return templateService.saveTestTemplate(dto);
    }

    public TemplatePart createTestPart(Long templateId, TemplatePartSaveDto dto){
        return templatePartService.createTestPart(templateId, dto);
    }

    public TemplateQuestionGroup createQuestionGroup(Long testPartId, TemplateQuestionGroupSaveDto dto){
        return templateQuestionGroupService.createQuestionGroup(testPartId, dto);
    }

    public TemplateQuestion createQuestion(Long questionGroupId, TemplateQuestionSaveDto dto) {
        TemplateQuestionGroup templateQuestionGroup = templateQuestionGroupDao.find(questionGroupId);
        return templateQuestionService.createQuestion(templateQuestionGroup, dto);
    }

    public TemplateQuestion updateQuestion(Long questionId, TemplateQuestionSaveDto dto) {
        return templateQuestionService.updateQuestion(questionId, dto);
    }

    public TemplateOption createOptionForQuestion(Long questionId, TemplateOptionSaveDto dto) {
        return templateOptionService.createSingleOptionForQuestion(questionId, dto);
    }

    public TemplatePossibleAnswer updateOption(Long possibleOptionId, TemplateOptionSaveDto dto) {
        return templateOptionService.updatePossibleAnswer(possibleOptionId, dto);
    }
}
