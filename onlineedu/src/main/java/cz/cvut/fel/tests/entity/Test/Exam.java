package cz.cvut.fel.tests.entity.Test;

import cz.cvut.fel.tests.entity.Template.Template;
import cz.cvut.kotyna.onlineedu.entity.Teacher;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class Exam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Date startDate;
    private Date endDate;
    private int duration;
    private int points;

    @ManyToOne
    @JoinColumn(name = "author", referencedColumnName = "id")
    private Teacher author;

    @OneToMany(mappedBy = "exam", cascade = CascadeType.ALL)
    private List<Test> tests;

    @ManyToOne
    @JoinColumn(name = "template", referencedColumnName = "id")
    private Template template;
}
