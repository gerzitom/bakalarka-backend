package cz.cvut.fel.tests.controller;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/ping")
@RequestScoped
public class PingController {

    @GET
    public Response ping(){
        return Response.ok("Ping").build();
    }

    @GET
    @Path("/teacher")
    @RolesAllowed({"teacher"})
    public Response pingTeacher(){
        return Response.ok("Ping").build();
    }
}
