package cz.cvut.fel.tests.dto.Template.QuestionGroup;

import cz.cvut.fel.tests.dto.Template.Question.TemplateQuestionReadDto;
import cz.cvut.fel.tests.entity.GroupType;
import cz.cvut.fel.tests.entity.Template.TemplateQuestionGroup;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TemplateQuestionGroupReadDto {
    private Long id;
    private String instructions;
    private GroupType groupType;
    private Long partId;
    private List<TemplateQuestionReadDto> questions;
    private int numberOfGeneratedQuestions;

    public TemplateQuestionGroupReadDto(TemplateQuestionGroup entity) {
        this.id = entity.getId();
        this.instructions = entity.getInstructions();
        this.numberOfGeneratedQuestions = entity.getNumberOfGeneratedQuestions();
        this.groupType = entity.getGroupType();
        this.partId = entity.getTemplatePart().getId();
        this.questions = entity.getTemplateQuestions().stream().map(TemplateQuestionReadDto::new).collect(Collectors.toList());
    }
}
