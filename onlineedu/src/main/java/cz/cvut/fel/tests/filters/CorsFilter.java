package cz.cvut.fel.tests.filters;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
public class CorsFilter implements ContainerResponseFilter {
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        containerResponseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
        containerResponseContext.getHeaders().add("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
        containerResponseContext.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
        containerResponseContext.getHeaders().add("Access-Control-Max-Age", "10");  // # of seconds
    }
}

//@Provider
//public class CorsFilter implements ContainerResponseFilter {
//
//    @Override
//    public void filter(ContainerRequestContext requestContext,
//                       ContainerResponseContext responseContext) throws IOException {
//        responseContext.getHeaders().add(
//                "Access-Control-Allow-Origin", "*");
////        responseContext.getHeaders().add(
////                "Access-Control-Allow-Credentials", "true");
////        responseContext.getHeaders().add(
////                "Access-Control-Allow-Headers",
////                "origin, content-type, accept, authorization");
////        responseContext.getHeaders().add(
////                "Access-Control-Allow-Methods",
////                "GET, POST, PUT, DELETE, OPTIONS, HEAD");
//    }
//}
