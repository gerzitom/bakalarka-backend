package cz.cvut.fel.tests.dto.Test;

import cz.cvut.fel.tests.entity.Test.Question;
import cz.cvut.fel.tests.entity.options.AnswerType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class QuestionReadDto {
    private Long id;
    private String content;
    private AnswerType answerType;
    private List<PossibleAnswerReadDto> possibleAnswers;
    private Double points;

    public QuestionReadDto(Question entity) {
        this.id = entity.getId();
        this.content = entity.getContent();
        this.answerType = entity.getAnswerType();
        this.possibleAnswers = entity.getPossibleAnswers().stream().map(PossibleAnswerReadDto::new).collect(Collectors.toList());
        this.points = entity.getPoints();
    }
}
