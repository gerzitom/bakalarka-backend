package cz.cvut.fel.tests.dao.Test;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Test.TestSummary;

import javax.ejb.Stateless;

@Stateless
public class TestSummaryDao extends GenericDao<TestSummary> {
    public TestSummaryDao(){
        super(TestSummary.class);
    }
}
