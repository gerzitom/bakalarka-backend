package cz.cvut.fel.tests.dao.Template;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Template.TemplateOption;

import javax.ejb.Stateless;

@Stateless
public class SingleOptionDao extends GenericDao<TemplateOption> {
    public SingleOptionDao(){
        super(TemplateOption.class);
    }
}
