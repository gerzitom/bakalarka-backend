package cz.cvut.fel.tests.service;

import cz.cvut.fel.tests.dao.ClassRoomDao;
import cz.cvut.fel.tests.dto.ClassRoomSaveDto;
import cz.cvut.fel.tests.dto.ClassroomReadDto;
import cz.cvut.kotyna.onlineedu.entity.Classroom;
import cz.cvut.kotyna.onlineedu.entity.Student;
import cz.cvut.kotyna.onlineedu.service.ClassroomService;
import cz.cvut.kotyna.onlineedu.service.StudentService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class ClassRoomServiceTests {

    @EJB
    private ClassRoomDao classRoomDao;

    @EJB
    private StudentService studentService;

    @EJB
    private ClassroomService classroomService;

    public ClassroomReadDto createClassroom(ClassRoomSaveDto dto){
        Classroom classroom = toEntityFromDto(dto);
        classRoomDao.save(classroom);
        return new ClassroomReadDto(classroom);
    }

    public Classroom toEntityFromDto(ClassRoomSaveDto dto){
        Classroom newClassroom = new Classroom();
        newClassroom.setName(dto.getName());
        return newClassroom;
    }

    public List<ClassroomReadDto> getAllClassRooms(){
        return classRoomDao.getAllClassRooms().stream().map(ClassroomReadDto::new)
                .collect(Collectors.toList());
    }

    public ClassroomReadDto getClassRoom(Long id) {
        Classroom classroom = classRoomDao.find(id);
        return new ClassroomReadDto(classroom);
    }

    public ClassroomReadDto addStudent(Long classId, Integer studentId) {
        Classroom classroom = classRoomDao.find(classId);
        Student student = studentService.findStudent(studentId);
        Collection<Student> students = classroom.getStudentCollection();
        students.add(student);
        classroom.setStudentCollection(students);
        classroomService.saveClassroom(classroom);
        return new ClassroomReadDto(classroom);
    }
}
