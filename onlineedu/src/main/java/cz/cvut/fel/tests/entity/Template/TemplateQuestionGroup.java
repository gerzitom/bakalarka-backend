package cz.cvut.fel.tests.entity.Template;

import cz.cvut.fel.tests.entity.GroupType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
public class TemplateQuestionGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String instructions;
    private int points;
    @Enumerated(EnumType.STRING)
    private GroupType groupType;
    private int numberOfGeneratedQuestions;

    @ManyToOne
    @JoinColumn(name = "templatePart", referencedColumnName = "id")
    private TemplatePart templatePart;

    @OneToMany(mappedBy = "templateQuestionGroup", cascade = CascadeType.ALL)
    private List<TemplateQuestion> templateQuestions;

}
