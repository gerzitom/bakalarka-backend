package cz.cvut.fel.tests.service.Test;

import cz.cvut.fel.tests.dao.Test.OptionDao;
import cz.cvut.fel.tests.entity.Template.TemplatePossibleAnswer;
import cz.cvut.fel.tests.entity.Test.Option;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class OptionService {
    @EJB
    private OptionDao generatedOptioDao;
    public Option generateOption(TemplatePossibleAnswer templatePossibleAnswer) {
        Option option = new Option(
                templatePossibleAnswer.getOption().getId(),
                templatePossibleAnswer.getOption().getAdditionalInfo(),
                templatePossibleAnswer.getOption().getText());
        generatedOptioDao.save(option);
        return option;
    }

}
