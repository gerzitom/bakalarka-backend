package cz.cvut.fel.tests.service;

import cz.cvut.fel.tests.dto.teaching.TeachingReadDto;
import cz.cvut.fel.tests.dto.teaching.TeachingSaveDto;
import cz.cvut.kotyna.onlineedu.entity.Classroom;
import cz.cvut.kotyna.onlineedu.entity.Teacher;
import cz.cvut.kotyna.onlineedu.entity.Teaching;
import cz.cvut.kotyna.onlineedu.service.ClassroomService;
import cz.cvut.kotyna.onlineedu.service.TeacherService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Stateless
public class TeachingServiceTests {
    @EJB
    private cz.cvut.kotyna.onlineedu.service.TeachingService eduTeachingService;
    @EJB
    private TeacherService teacherService;
    @EJB
    private ClassroomService eduClassRoomService;

    public TeachingReadDto fromEntityToDto(Teaching entity){
        return new TeachingReadDto(entity);
    }

    public TeachingReadDto getTeaching(Integer id) {
        return fromEntityToDto(eduTeachingService.findTeaching(id));
    }

    public TeachingReadDto createTeaching(TeachingSaveDto dto){
        Teaching teaching = new Teaching();
        Teacher teacher = teacherService.findTeacher(1); //TODO predelat na currently logged in ucitele
        teaching.setClassroom(eduClassRoomService.findClassroom(dto.getClassroomId()));
        teaching.setTeacher(teacher);
        eduTeachingService.saveTeaching(teaching);
        return fromEntityToDto(teaching);
    }

    public List<TeachingReadDto> getTeachingCollection(Integer id){
        //TODO predelat na currently logged in ucitele
        Teacher teacher = teacherService.findTeacher(1);
        Collection<Teaching> teachingCollection = teacher.getTeachingCollection();
        List<TeachingReadDto> teachingReadDtos = new ArrayList<>();
        for(Teaching t : teachingCollection){
            teachingReadDtos.add(fromEntityToDto(t));
        }
        return teachingReadDtos;
    }

    public TeachingReadDto updateTeaching(Integer id, TeachingSaveDto dto) {
        Teaching teaching = eduTeachingService.findTeaching(id);
        Classroom classroom = eduClassRoomService.findClassroom(dto.getClassroomId());
        teaching.setClassroom(classroom);
        eduTeachingService.saveTeaching(teaching);
        return fromEntityToDto(teaching);
    }
}
