package cz.cvut.fel.tests.controller;

import cz.cvut.fel.tests.dto.Template.TemplateReadDto;
import cz.cvut.fel.tests.service.Template.TemplateService;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

@Path("/teachers")
public class TeacherController {

    @EJB
    private TemplateService templateService;

    @GET
    @Path("/{teacherId}/test_templates")
    public List<TemplateReadDto> getAllTestTemplates(@PathParam("teacherId") Long teacherId){
        return templateService.getTestTemplates(teacherId);
    }



}
