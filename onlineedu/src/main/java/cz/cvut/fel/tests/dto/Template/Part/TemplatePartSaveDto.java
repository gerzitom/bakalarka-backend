package cz.cvut.fel.tests.dto.Template.Part;

import cz.cvut.fel.tests.entity.TestTemplatePreferredType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class TemplatePartSaveDto {
    private String name;
    private int estimatedTime;
    private String description;
    private int points;
    private TestTemplatePreferredType preferredType;
}
