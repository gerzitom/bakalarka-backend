package cz.cvut.fel.tests.service.Test;

import cz.cvut.fel.tests.dao.Test.PossibleAnswerDao;
import cz.cvut.fel.tests.entity.Template.TemplatePossibleAnswer;
import cz.cvut.fel.tests.entity.Test.PossibleAnswer;
import cz.cvut.fel.tests.entity.Test.Question;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class PossibleAnswerService {
    @EJB
    private OptionService optionService;
    @EJB
    private PossibleAnswerDao possibleAnswerDao;
    @EJB
    public void generatePossibleAnswer(TemplatePossibleAnswer templatePossibleAnswer, Question question){
        PossibleAnswer possibleAnswer = new PossibleAnswer(
                templatePossibleAnswer.getId(), templatePossibleAnswer.getCorrect(),
                optionService.generateOption(templatePossibleAnswer), question);
        possibleAnswerDao.save(possibleAnswer);
    }
}
