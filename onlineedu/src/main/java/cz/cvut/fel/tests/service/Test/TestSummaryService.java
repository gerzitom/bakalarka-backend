package cz.cvut.fel.tests.service.Test;

import cz.cvut.fel.tests.dao.Test.TestSummaryDao;
import cz.cvut.fel.tests.entity.Test.Test;
import cz.cvut.fel.tests.entity.Test.TestSummary;
import cz.cvut.kotyna.onlineedu.entity.Student;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class TestSummaryService {
    @EJB
    private TestSummaryDao testSummaryDao;

    public void createTestSummary(Test test, Student student){
        TestSummary testSummary = new TestSummary();
        testSummary.setTest(test);
        testSummary.setStudent(student);
        testSummaryDao.save(testSummary);
    }


}
