package cz.cvut.fel.tests.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class GenericDao<Entity> {

    @PersistenceContext
    private EntityManager em;
    private final Class<Entity> entityClass;

    protected GenericDao(Class<Entity> entityClass) {
        this.entityClass = entityClass;
    }

    public Entity save(Entity entity){
        em.persist(entity);
        return entity;
    }

    public void delete(final Object id){
        this.em.remove(this.em.getReference(entityClass, id));
    }

    public Entity find(final Object id) {
        return (Entity) this.em.find(entityClass, id);
    }

    public Entity update(final Entity t) {
        return this.em.merge(t);
    }
}
