package cz.cvut.fel.tests.dto.Template.Question;

import cz.cvut.fel.tests.entity.TestTemplatePreferredType;
import cz.cvut.fel.tests.entity.options.AnswerType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TemplateQuestionSaveDto {
    private String content;
    private AnswerType answerType;
}
