package cz.cvut.fel.tests.dto;

import cz.cvut.fel.tests.dto.user.UserReadDto;
import cz.cvut.kotyna.onlineedu.entity.Classroom;
import cz.cvut.kotyna.onlineedu.entity.Student;
import cz.cvut.kotyna.onlineedu.entity.UserAccount;
import lombok.Getter;
import lombok.Setter;

import javax.xml.registry.infomodel.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class ClassroomReadDto {
    private Integer id;
    private String name;
    private List<UserReadDto> students;

    public ClassroomReadDto(Classroom entity){
        this.id = entity.getId();
        this.name = entity.getName();
        this.students = getStudents(entity).stream().map(UserReadDto::new).collect(Collectors.toList());
    }

    public Collection<UserAccount> getStudents(Classroom entity){
        Collection<Student> students =  entity.getStudentCollection();
        Collection<UserAccount> users = new ArrayList<>();
        for(Student s : students){
           users.add(s.getUserAccount());
        }
        return users;
    }
}
