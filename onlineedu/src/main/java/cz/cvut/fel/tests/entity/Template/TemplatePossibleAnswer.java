package cz.cvut.fel.tests.entity.Template;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class TemplatePossibleAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Boolean correct;

    @ManyToOne(cascade = {CascadeType.REMOVE, CascadeType.REFRESH})
    @JoinColumn(name = "option", referencedColumnName = "id")
    private TemplateOption option;

    @ManyToOne
    @JoinColumn(name = "question", referencedColumnName = "id")
    private TemplateQuestion templateQuestion;

}
