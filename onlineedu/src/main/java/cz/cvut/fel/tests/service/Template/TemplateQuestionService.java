package cz.cvut.fel.tests.service.Template;

import cz.cvut.fel.tests.dao.Template.TemplateQuestionDao;
import cz.cvut.fel.tests.dao.Template.TemplateQuestionGroupDao;
import cz.cvut.fel.tests.dto.Template.Question.TemplateQuestionReadDto;
import cz.cvut.fel.tests.dto.Template.Question.TemplateQuestionSaveDto;
import cz.cvut.fel.tests.entity.Template.TemplateQuestion;
import cz.cvut.fel.tests.entity.Template.TemplateQuestionGroup;
import cz.cvut.fel.tests.exceptions.BasicException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class TemplateQuestionService {

    @EJB
    private TemplateQuestionDao templateQuestionDao;

    @EJB
    private TemplateQuestionGroupDao templateQuestionGroupDao;

//    public ErrorMessageDto checkQuestion(QuestionGroup questionGroup){
//        List<Question> questions = questionGroup.getQuestions();
//        int questionListSize = questions.size();
//        if(questionGroup.getGroupType().equals("SIMPLE") && questionListSize >= 1){
//            return new ErrorMessageDto("bla");
//        }
//        return null;
//    }
//
    public TemplateQuestion createQuestion(TemplateQuestionGroup templateQuestionGroup, TemplateQuestionSaveDto dto){
        TemplateQuestion newTemplateQuestion = toEntityFromDto(dto);
        newTemplateQuestion.setAnswerType(templateQuestionGroup.getTemplatePart().getPreferredType().getAnswerType());
        newTemplateQuestion.setTemplateQuestionGroup(templateQuestionGroup);
        List<TemplateQuestion> templateQuestionList = templateQuestionGroup.getTemplateQuestions();
        int futureNumberOfQuestions = templateQuestionList.size() + 1;
        Double points = (double) templateQuestionGroup.getPoints() / futureNumberOfQuestions;
        for(TemplateQuestion q : templateQuestionList){
            q.setPoints(points);
        }
        newTemplateQuestion.setPoints(points);
        templateQuestionDao.save(newTemplateQuestion);
        return newTemplateQuestion;
    }

    public TemplateQuestion createQuestion(TemplateQuestionGroup templateQuestionGroup){
        TemplateQuestionSaveDto saveDto = new TemplateQuestionSaveDto("", null);
        return createQuestion(templateQuestionGroup, saveDto);
    }

    public TemplateQuestion updateQuestion(Long questionId, TemplateQuestionSaveDto dto){
        TemplateQuestion templateQuestion = templateQuestionDao.find(questionId);
        templateQuestion.setContent(dto.getContent());
        templateQuestion.setAnswerType(dto.getAnswerType());
        templateQuestionDao.update(templateQuestion);
        return templateQuestion;
    }

    public TemplateQuestion toEntityFromDto(TemplateQuestionSaveDto dto){
        TemplateQuestion newTemplateQuestion = new TemplateQuestion();
        newTemplateQuestion.setContent(dto.getContent());
        return newTemplateQuestion;
    }

    public void deleteQuestion(Long questionId){
        if(templateQuestionDao.find(questionId) == null){
            throw new BasicException("Otazka nebyla nalezena");
        }
        templateQuestionDao.delete(questionId);
    }
}
