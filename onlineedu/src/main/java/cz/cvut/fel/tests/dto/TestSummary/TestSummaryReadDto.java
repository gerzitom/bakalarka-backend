package cz.cvut.fel.tests.dto.TestSummary;

import cz.cvut.fel.tests.dto.RESTTimestampParam;
import cz.cvut.fel.tests.dto.user.UserReadDto;
import cz.cvut.fel.tests.entity.Test.TestSummary;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TestSummaryReadDto {
    private Long id;
    private String timeStarted;
    private String timeFinished;
    private UserReadDto student;

    private Long testId;
    private double numberOfPoints;
    private int maxPoints;

    public TestSummaryReadDto(TestSummary entity){
        this.id = entity.getId();
        this.timeStarted = entity.getTimeStarted() != null ? new RESTTimestampParam(entity.getTimeStarted()).toString() : null;
        this.timeFinished = entity.getTimeFinished() != null ?  new RESTTimestampParam(entity.getTimeFinished()).toString() : null;
        this.numberOfPoints = entity.getAmountOfPoints();
        this.student = new UserReadDto(entity.getStudent().getUserAccount());
        this.maxPoints = entity.getTest().getExam().getPoints();
        this.testId = entity.getTest().getId();
    }




}
