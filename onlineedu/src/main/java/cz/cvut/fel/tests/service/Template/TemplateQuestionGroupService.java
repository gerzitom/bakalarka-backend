package cz.cvut.fel.tests.service.Template;

import cz.cvut.fel.tests.dao.Template.TemplateDao;
import cz.cvut.fel.tests.dao.Template.TemplateQuestionGroupDao;
import cz.cvut.fel.tests.dao.Template.TemplatePartDao;
import cz.cvut.fel.tests.dto.Template.QuestionGroup.TemplateQuestionGroupReadDto;
import cz.cvut.fel.tests.dto.Template.QuestionGroup.TemplateQuestionGroupSaveDto;
import cz.cvut.fel.tests.entity.GroupType;
import cz.cvut.fel.tests.entity.Template.TemplateQuestionGroup;
import cz.cvut.fel.tests.entity.Template.TemplatePart;
import cz.cvut.fel.tests.entity.Test.Question;
import cz.cvut.fel.tests.entity.Test.QuestionGroup;
import cz.cvut.fel.tests.exceptions.BasicException;

import javax.ejb.EJB;
import javax.ejb.Stateless;

;import java.util.ArrayList;
import java.util.List;

@Stateless
public class TemplateQuestionGroupService {

    @EJB
    private TemplateQuestionService templateQuestionService;
    @EJB
    private TemplatePartDao templatePartDao;
    @EJB
    private TemplateQuestionGroupDao templateQuestionGroupDao;
    @EJB
    private TemplateDao templateDao;

    public TemplateQuestionGroup createQuestionGroup(Long testPartId, TemplateQuestionGroupSaveDto dto) {
        TemplateQuestionGroup newTemplateQuestionGroup = toEntityFromDto(dto);
        TemplatePart templatePart = templatePartDao.find(testPartId);
        newTemplateQuestionGroup.setTemplatePart(templatePart);
        newTemplateQuestionGroup.setPoints(templatePart.getPoints());
        newTemplateQuestionGroup.setGroupType(templatePart.getPreferredType().getGroupType());
        templateQuestionGroupDao.save(newTemplateQuestionGroup);
        if(newTemplateQuestionGroup.getGroupType().equals(GroupType.SIMPLE) || newTemplateQuestionGroup.getGroupType().equals(GroupType.OPEN)) {
            templateQuestionService.createQuestion(newTemplateQuestionGroup);
        }
        return newTemplateQuestionGroup;
    }

    public TemplateQuestionGroup updateQuestionGroup(Long groupId, TemplateQuestionGroupSaveDto dto) {
        TemplateQuestionGroup templateQuestionGroup = templateQuestionGroupDao.find(groupId);
        updateFromDto(templateQuestionGroup, dto);
        templateQuestionGroupDao.update(templateQuestionGroup);
        return templateQuestionGroup;
    }

    private void updateFromDto(TemplateQuestionGroup templateQuestionGroup, TemplateQuestionGroupSaveDto dto) {
        templateQuestionGroup.setInstructions(dto.getInstructions());
        templateQuestionGroup.setNumberOfGeneratedQuestions(dto.getNumberOfGeneratedQuestions());
    }

    public TemplateQuestionGroup toEntityFromDto(TemplateQuestionGroupSaveDto saveDto){
        TemplateQuestionGroup templateQuestionGroup = new TemplateQuestionGroup();
        templateQuestionGroup.setInstructions(saveDto.getInstructions());
        templateQuestionGroup.setGroupType(saveDto.getGroupType());
        templateQuestionGroup.setNumberOfGeneratedQuestions(saveDto.getNumberOfGeneratedQuestions());
        return templateQuestionGroup;
    }

    public void deleteQuestionGroup(Long groupId) {
        if(templateQuestionGroupDao.find(groupId) == null){
            throw new BasicException("Skupina otazek nebyla nalezena");
        }
        templateQuestionGroupDao.delete(groupId);
    }

    public List<TemplateQuestionGroup> getGroups(Long templateId) {
        List<TemplatePart> templateParts = templateDao.find(templateId).getTemplateParts();
        List<TemplateQuestionGroup> questionGroups = new ArrayList<>();
        for(TemplatePart templatePart : templateParts){
            questionGroups.addAll(templatePart.getTemplateQuestionGroups());
        }
        return questionGroups;
    }
}
