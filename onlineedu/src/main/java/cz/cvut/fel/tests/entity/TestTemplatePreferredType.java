package cz.cvut.fel.tests.entity;

import cz.cvut.fel.tests.entity.options.AnswerType;

public enum TestTemplatePreferredType {
    ONE_OPTION(AnswerType.ONE_OPTION, GroupType.SIMPLE),
    MULTIPLE_OPTION(AnswerType.MULTIPLE_OPTIONS, GroupType.SIMPLE),
    TEXT_AREA(AnswerType.OPENANSWER, GroupType.OPEN),
    SPOJOVACKA(AnswerType.ONE_OPTION, GroupType.SPOJOVACKA),
    FILL_GAPS(AnswerType.ONE_OPTION, GroupType.FILL_GAPS);

    private AnswerType answerType;
    private GroupType groupType;

    TestTemplatePreferredType(AnswerType answerType, GroupType groupType) {
        this.answerType = answerType;
        this.groupType = groupType;
    }

    public AnswerType getAnswerType(){
        return answerType;
    }

    public GroupType getGroupType() {
        return groupType;
    }
}
