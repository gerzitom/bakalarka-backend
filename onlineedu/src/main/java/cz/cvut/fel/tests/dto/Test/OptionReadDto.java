package cz.cvut.fel.tests.dto.Test;

import cz.cvut.fel.tests.entity.Test.Option;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OptionReadDto {
    private Long id;
    private String text;
    private String additionalInfo;

    public OptionReadDto(Option entity) {
        this.id = entity.getId();
        this.text = entity.getText();
        this.additionalInfo = entity.getAdditionalInfo();
    }
}
