package cz.cvut.fel.tests.dto.Test;


import cz.cvut.fel.tests.dto.RESTTimestampParam;
import cz.cvut.fel.tests.entity.Test.Exam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExamReadDto {
    private Long id;
    private String name;
    private String startDate;
    private String endDate;

    public ExamReadDto(Exam entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        this.startDate = new RESTTimestampParam(entity.getStartDate()).toString();
        this.endDate = new RESTTimestampParam(entity.getEndDate()).toString();
    }
}
