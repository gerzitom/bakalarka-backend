package cz.cvut.fel.tests.dto.Test;

import cz.cvut.fel.tests.entity.Test.TestPart;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestPartReadDto {
    private Long id;
    private QuestionGroupReadDto questionGroup;

    public TestPartReadDto(TestPart entity){
        this.id = entity.getId();
        this.questionGroup = new QuestionGroupReadDto(entity.getQuestionGroup());
    }
}
