package cz.cvut.fel.tests.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class ErrorMessageDto implements Serializable {
    private String message;

    public ErrorMessageDto(String message) {
        this.message = message;
    }
}
