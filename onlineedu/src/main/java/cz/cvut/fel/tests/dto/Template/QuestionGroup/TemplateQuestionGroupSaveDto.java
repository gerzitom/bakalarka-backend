package cz.cvut.fel.tests.dto.Template.QuestionGroup;

import cz.cvut.fel.tests.entity.GroupType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TemplateQuestionGroupSaveDto {
    private String instructions;
    private GroupType groupType;
    private int numberOfGeneratedQuestions;
}
