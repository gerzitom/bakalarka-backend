package cz.cvut.fel.tests.dto.CorrectAndUserAnswers;

import cz.cvut.fel.tests.dto.TestSummary.TestSummaryReadDto;
import cz.cvut.fel.tests.entity.Test.Test;
import cz.cvut.fel.tests.entity.Test.TestPart;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TestAnswers {
    private List<UserAnswersDto> userAnswers;
    private List<RightAnswersDto> rightAnswers;
    private TestSummaryReadDto testSummaryReadDto;
    private boolean teacherMarkingRequired;

    public TestAnswers (Test entity){
        this.testSummaryReadDto = new TestSummaryReadDto(entity.getTestSummaries().get(0));
        this.userAnswers = entity.getTestParts().stream().map(TestPart::getQuestionGroup).map(UserAnswersDto::new).collect(Collectors.toList());
        this.rightAnswers = entity.getTestParts().stream().map(TestPart::getQuestionGroup).map(RightAnswersDto::new).collect(Collectors.toList());
        this.teacherMarkingRequired = entity.isTeacherMarkingRequired();
    }
}
