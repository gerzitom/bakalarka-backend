package cz.cvut.fel.tests.entity;

import cz.cvut.fel.tests.entity.options.AnswerType;

public abstract class QuestionAnswers {
    private AnswerType type;

    public QuestionAnswers(AnswerType type) {
        this.type = type;
    }

    public abstract String toJson();
}
