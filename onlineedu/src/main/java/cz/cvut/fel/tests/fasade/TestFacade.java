package cz.cvut.fel.tests.fasade;

import cz.cvut.fel.tests.dao.ClassRoomDao;
import cz.cvut.fel.tests.dao.Test.TestDao;
import cz.cvut.fel.tests.dao.Template.TemplateDao;
import cz.cvut.fel.tests.dto.Test.ExamSaveDto;
import cz.cvut.fel.tests.entity.Template.TemplateQuestionGroup;
import cz.cvut.fel.tests.entity.Template.TemplatePart;
import cz.cvut.fel.tests.entity.Test.Exam;
import cz.cvut.fel.tests.entity.Test.QuestionGroup;
import cz.cvut.fel.tests.entity.Test.Test;
import cz.cvut.fel.tests.entity.Test.TestPart;
import cz.cvut.fel.tests.exceptions.BasicException;
import cz.cvut.fel.tests.service.Template.TemplatePartService;
import cz.cvut.fel.tests.service.Template.TemplateService;
import cz.cvut.fel.tests.service.Test.ExamService;
import cz.cvut.fel.tests.service.Test.TestPartService;
import cz.cvut.fel.tests.service.Test.TestService;
import cz.cvut.fel.tests.service.Test.TestSummaryService;
import cz.cvut.kotyna.onlineedu.entity.Classroom;
import cz.cvut.kotyna.onlineedu.entity.Student;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Stateless
public class TestFacade {
    @EJB
    private TemplateService templateService;
    @EJB
    private TestService testService;
    @EJB
    private TestDao testDao;
    @EJB
    private TestPartService testPartService;
    @EJB
    private TemplatePartService templatePartService;
    @EJB
    private ClassRoomDao classroomDao;
    @EJB
    private TestSummaryService testSummaryService;
    @EJB
    private TemplateDao templateDao;
    @EJB
    private ExamService examService;

    public List<Test> generateTest(ExamSaveDto dto, Long templateId) throws BasicException {
        if(templateDao.find(templateId) == null){
            throw new BasicException("You have to create a template in order to generate tests");
        }
        return createTests(dto, templateId);
    }

    /**
     * Returns a list of tests based on a specific template. A parent entity Exam
     * is created for all tests. A different test is created
     * for each student present in the classroom. The method randomly
     * selects one TemplateQuestionGroup from each TemplatePart, copies its
     * data into a new test QuestionGroup.
     * @param dto information about the Exam, like the startDate, name etc.
     * @param templateId the Template based on which the tests are created
     * @return the list of created Tests
     */
    public List<Test> createTests(ExamSaveDto dto, Long templateId) {
        List<TemplatePart> templatePartList = templateService.getTestParts(templateId);
        Classroom classroom = classroomDao.find(dto.getClassroomId());
        List<Student> students = (List<Student>) classroom.getStudentCollection();
        int maxPoints = 0;
        for(TemplatePart t : templatePartList){
            maxPoints += t.getPoints();
        }
        Exam exam = examService.createExam(dto, templateId, maxPoints);
        List<Test> tests = new ArrayList<>();
        for(Student s : students) {
            Test test = testService.createTest(exam);
            testSummaryService.createTestSummary(test, s);
            test.setClassroom(classroom);
            Random rand = new Random();
            for (TemplatePart t : templatePartList) {
                List<TemplateQuestionGroup> templateQuestionGroups = templatePartService.getQuestionsGroups(t.getId());
                if(templateQuestionGroups.isEmpty()){
                    throw new BasicException("No question groups present in the template");
                } else {
                    TemplateQuestionGroup result = templateQuestionGroups.get(rand.nextInt(templateQuestionGroups.size()));
                    testPartService.createNewTestPart(result, test);
                }
            }
            testDao.save(test);
            tests.add(test);
        }
        return tests;
    }

    public List<QuestionGroup> getQuestionGroups(Long testId){
        List<TestPart> testParts = testDao.find(testId).getTestParts();
        List<QuestionGroup> result = new ArrayList<>();
        for(TestPart t : testParts){
            result.add(t.getQuestionGroup());
        }
        return result;
    }
}
