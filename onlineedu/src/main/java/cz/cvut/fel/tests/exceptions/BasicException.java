package cz.cvut.fel.tests.exceptions;

import cz.cvut.fel.tests.dto.ErrorMessageDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@javax.ejb.ApplicationException(rollback=true)
public class BasicException extends Error implements Serializable {
    private String message;
    private int code;

    public BasicException(String message) {
        super(message);
        this.message = message;
        this.code = 400;
    }

    public ErrorMessageDto getDto(){
        return new ErrorMessageDto(message);
    }
}
