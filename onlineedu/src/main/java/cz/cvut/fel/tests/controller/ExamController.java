package cz.cvut.fel.tests.controller;

import cz.cvut.fel.tests.dto.Test.ExamReadDto;
import cz.cvut.fel.tests.dto.Test.ExamSaveDto;
import cz.cvut.fel.tests.dto.TestSummary.TestSummaryReadDto;
import cz.cvut.fel.tests.service.Test.ExamService;
import cz.cvut.fel.tests.service.Test.TestService;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import java.util.List;
import java.util.stream.Collectors;

@RequestScoped
@RolesAllowed({"teacher"})
@Path("/exams")
public class ExamController {
    @EJB
    private ExamService examService;

    @EJB
    private TestService testService;

    @GET
    public List<ExamReadDto> getAllExams(){
        return examService.getAllExams().stream().map(ExamReadDto::new).collect(Collectors.toList());
    }

    @GET
    @Path("/{examId}")
    public ExamReadDto getExam(
            @PathParam("examId") Long examId
    ){
        return new ExamReadDto(examService.getExam(examId));
    }

    @PUT
    @Path("/{examId}")
    public ExamReadDto updateExam(
            @RequestBody ExamSaveDto dto,
            @PathParam("examId") Long examId
    ){
        return new ExamReadDto(examService.updateExam(dto, examId));
    }

    @DELETE
    @Path("/{examId}")
    public void deleteExam(
            @PathParam("examId") Long examId
    ){
        examService.deleteExam(examId);
    }

    @GET
    @Path("/{examId}/submittedTests")
    public List<TestSummaryReadDto>  getSubmittedTests(
            @PathParam("examId") Long examId
    ){
       return testService.getSubmittedTests(examId)
                .stream().map(TestSummaryReadDto::new)
                .collect(Collectors.toList());
    }

    @GET
    @Path("{examId}/allTests")
    public List<TestSummaryReadDto>  getAllTests(
            @PathParam("examId") Long examId
    ){
        return examService.getTests(examId)
                .stream().map(TestSummaryReadDto::new)
                .collect(Collectors.toList());
    }

}
