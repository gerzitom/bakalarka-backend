package cz.cvut.fel.tests.dto.Template.PossibleAnswer;

import cz.cvut.fel.tests.dto.Template.Option.TemplateOptionReadDto;
import cz.cvut.fel.tests.entity.Template.TemplatePossibleAnswer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TemplatePossibleAnswerReadDto {
    private Long id;
    private TemplateOptionReadDto singleOption;
    private boolean correct;

    public TemplatePossibleAnswerReadDto(TemplatePossibleAnswer entity){
        this.id = entity.getId();
        this.correct = entity.getCorrect();
        this.singleOption = new TemplateOptionReadDto(entity.getOption());
    }
}
