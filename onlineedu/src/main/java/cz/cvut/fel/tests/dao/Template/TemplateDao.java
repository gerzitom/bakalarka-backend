package cz.cvut.fel.tests.dao.Template;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Template.Template;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class TemplateDao extends GenericDao<Template> {
    @PersistenceContext
    EntityManager em;

    public TemplateDao() {
        super(Template.class);
    }

    public List<Template> getTemplatesByAuthor(Long authorId){
        return em.createNamedQuery(Template.QUERY_FINDBYAUTHOR, Template.class)
                .setParameter("authorId", authorId)
                .getResultList();
    }

    public List<Template> getTemplatesByName(String name){
        return em.createNamedQuery(Template.QUERY_FINDBYNAME, Template.class)
                .setParameter("name", name)
                .getResultList();
    }
}
