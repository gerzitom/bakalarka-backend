package cz.cvut.fel.tests.controller;

import cz.cvut.fel.tests.dto.Template.Option.TemplateOptionReadDto;
import cz.cvut.fel.tests.dto.Template.Option.TemplateOptionSaveDto;
import cz.cvut.fel.tests.dto.Template.Part.TemplatePartReadDto;
import cz.cvut.fel.tests.dto.Template.Part.TemplatePartSaveDto;
import cz.cvut.fel.tests.dto.Template.PossibleAnswer.TemplatePossibleAnswerReadDto;
import cz.cvut.fel.tests.dto.Template.Question.TemplateQuestionReadDto;
import cz.cvut.fel.tests.dto.Template.Question.TemplateQuestionSaveDto;
import cz.cvut.fel.tests.dto.Template.QuestionGroup.TemplateQuestionGroupReadDto;
import cz.cvut.fel.tests.dto.Template.QuestionGroup.TemplateQuestionGroupSaveDto;
import cz.cvut.fel.tests.dto.Template.TemplateReadDto;
import cz.cvut.fel.tests.dto.Template.TemplateSaveDto;
import cz.cvut.fel.tests.fasade.TestTemplateFacade;
import cz.cvut.fel.tests.service.Template.*;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;
import java.util.stream.Collectors;

@Path("/test_templates")
@RolesAllowed({"teacher"})
public class TemplateController {
    @EJB
    private TemplateService templateService;

    @EJB
    private TestTemplateFacade testTemplateFacade;

    @EJB
    private TemplatePartService templatePartService;

    @EJB
    private TemplateQuestionGroupService templateQuestionGroupService;

    @EJB
    private TemplateQuestionService templateQuestionService;

    @EJB
    private TemplateOptionService templateOptionService;

    @POST
    public TemplateReadDto saveTestTemplate(TemplateSaveDto dto){
        return new TemplateReadDto(testTemplateFacade.saveTestTemplate(dto));
    }

    @GET
    public List<TemplateReadDto> getTestTemplates(){
        return templateService.getTestTemplates().stream().map(TemplateReadDto::new)
                .collect(Collectors.toList());
    }

    @GET
    @Path("/{templateId}")
    public TemplateReadDto getTestTemplate(
            @PathParam("templateId") Long templateId){
        return new TemplateReadDto(templateService.getTestTemplate(templateId));
    }

    @PUT
    @Path("/{templateId}")
    public TemplateReadDto updateTestTemplate(
            @RequestBody TemplateSaveDto dto,
            @PathParam("templateId") Long id
    ){
        return new TemplateReadDto(templateService.updateTestTemplate(dto, id));
    }

    @DELETE
    @Path("/{templateId}")
    public void deleteTestTemplate(
            @PathParam("templateId") Long id
    ){
        templateService.deleteTestTemplate(id);
    }

    @POST
    @Path("/{templateId}/parts")
    public TemplatePartReadDto createTemplatePart(
            @RequestBody TemplatePartSaveDto templatePartSaveDto,
            @PathParam("templateId") Long templateId
    ){
        return new TemplatePartReadDto(testTemplateFacade.createTestPart(templateId, templatePartSaveDto));
    }

    @PUT
    @Path("parts/{partId}")
    public TemplatePartReadDto updateTemplatePart(
            @RequestBody TemplatePartSaveDto templatePartSaveDto,
            @PathParam("partId") Long partId
    ){
        return new TemplatePartReadDto(templatePartService.updateTestPart(templatePartSaveDto, partId));
    }

    @DELETE
    @Path("parts/{partId}")
    public void deleteTemplatePart(
            @PathParam("partId") Long partId
    ){
        templatePartService.deleteTemplatePart(partId);
    }

    @POST
    @Path("/parts/{testPartId}/group")
    public TemplateQuestionGroupReadDto createQuestionGroup(
            @RequestBody TemplateQuestionGroupSaveDto templateQuestionGroupSaveDto,
            @PathParam("testPartId") Long testPartId
    ) {
        return new TemplateQuestionGroupReadDto(testTemplateFacade.createQuestionGroup(testPartId, templateQuestionGroupSaveDto));
    }

    @PUT
    @Path("/parts/group/{groupId}")
    public TemplateQuestionGroupReadDto updateQuestionGroup(
            @RequestBody TemplateQuestionGroupSaveDto templateQuestionGroupSaveDto,
            @PathParam("groupId") Long groupId
    ) {
        return new TemplateQuestionGroupReadDto(
                templateQuestionGroupService.updateQuestionGroup(groupId, templateQuestionGroupSaveDto));
    }

    @DELETE
    @Path("/parts/group/{groupId}")
    public void deleteTemplateQuestionGroup(
            @PathParam("groupId") Long groupId
    ) {
        templateQuestionGroupService.deleteQuestionGroup(groupId);
    }

    @POST
    @Path("/parts/group/{questionGroupId}/question")
    public TemplateQuestionReadDto createTemplateQuestion(
            @RequestBody TemplateQuestionSaveDto dto,
            @PathParam("questionGroupId") Long questionGroupId
    ){
        return new TemplateQuestionReadDto(testTemplateFacade.createQuestion(questionGroupId, dto));
    }

    @PUT
    @Path("/parts/groups/question/{questionId}")
    public TemplateQuestionReadDto updateTemplateQuestion(
            @RequestBody TemplateQuestionSaveDto dto,
            @PathParam("questionId") Long questionId
    ){
        return new TemplateQuestionReadDto(testTemplateFacade.updateQuestion(questionId, dto));
    }

    @DELETE
    @Path("/parts/groups/question/{questionId}")
    public void deleteTemplateQuestion(
            @PathParam("questionId") Long questionId
    ){
        templateQuestionService.deleteQuestion(questionId);
    }


    @POST
    @Path("/questions/{questionId}/option")
    public TemplateOptionReadDto createOptionForQuestion(
            @RequestBody TemplateOptionSaveDto dto,
            @PathParam("questionId") Long questionId
    ){
       return new TemplateOptionReadDto(
               testTemplateFacade.createOptionForQuestion(questionId, dto));
    }

    @PUT
    @Path("/questions/option/{optionId}")
    public TemplatePossibleAnswerReadDto updateOption(
            @RequestBody TemplateOptionSaveDto dto,
            @PathParam("optionId") Long optionId
    ){
        return new TemplatePossibleAnswerReadDto(testTemplateFacade.updateOption(optionId, dto));
    }

    @DELETE
    @Path("/questions/option/{optionId}")
    public void deleteOption(
            @PathParam("optionId") Long optionId
    ){
        templateOptionService.deleteOption(optionId);
    }

    @GET
    @Path("{templateId}/groups")
    public List<TemplateQuestionGroupReadDto> getGroupsWithQuestions(
            @PathParam("templateId") Long templateId
    ){
        return templateQuestionGroupService.getGroups(templateId)
                .stream().map(TemplateQuestionGroupReadDto::new)
                .collect(Collectors.toList());
    }

    @GET
    @Path("{templateId}/templateparts")
    public List<TemplatePartReadDto> getTestTemplateParts(
            @PathParam("templateId") Long templateId
    ){
        return templateService.getTestReadParts(templateId)
                .stream().map(TemplatePartReadDto::new).collect(Collectors.toList());
    }

    @GET
    @Path("test_template_parts/{partId}")
    public TemplatePartReadDto getTestTemplatePartQuestions(
            @PathParam("partId") Long partId
    ){
        return templateService.getTestPartQuestions(partId);
    }

}
