package cz.cvut.fel.tests.dao.Test;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Test.Question;

import javax.ejb.Stateless;

@Stateless
public class QuestionDao extends GenericDao<Question> {
    public QuestionDao(){
        super(Question.class);
    }
}
