package cz.cvut.fel.tests.service.Template;

import cz.cvut.fel.tests.dao.Template.TemplateDao;
import cz.cvut.fel.tests.dao.Template.TemplatePartDao;
import cz.cvut.fel.tests.dto.Template.Part.TemplatePartSaveDto;
import cz.cvut.fel.tests.entity.Template.TemplateQuestionGroup;
import cz.cvut.fel.tests.entity.Template.TemplatePart;
import cz.cvut.fel.tests.exceptions.BasicException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.transaction.Transactional;
import java.util.List;

@Stateless
public class TemplatePartService {

    @EJB
    private TemplatePartDao templatePartDao;

    @EJB
    private TemplateDao templateDao;

    @Transactional
    public TemplatePart createTestPart(Long templateId, TemplatePartSaveDto dto){
        TemplatePart newTemplatePart = toEntityFromDto(dto);
        newTemplatePart.setTemplate(templateDao.find(templateId));
        templatePartDao.save(newTemplatePart);
        return newTemplatePart;
    }

    public TemplatePart toEntityFromDto(TemplatePartSaveDto dto){
        TemplatePart newTemplatePart = new TemplatePart();
        newTemplatePart.setName(dto.getName());
        newTemplatePart.setDescription(dto.getDescription());
        newTemplatePart.setEstimatedTime(dto.getEstimatedTime());
        newTemplatePart.setPreferredType(dto.getPreferredType());
        newTemplatePart.setPoints(dto.getPoints());
        return newTemplatePart;
    }

    public List<TemplateQuestionGroup> getQuestionsGroups(Long TestPartId){
        TemplatePart templatePart = templatePartDao.find(TestPartId);
        return templatePart.getTemplateQuestionGroups();
    }

    public void deleteTemplatePart(Long partId) {
        if (templatePartDao.find(partId) == null){
            throw new BasicException("Testova cast nebyla nalezena");
        }
        templatePartDao.delete(partId);
    }

    public TemplatePart updateTestPart(TemplatePartSaveDto templatePartSaveDto, Long partId) {
        TemplatePart templatePart = templatePartDao.find(partId);
        if(templatePart == null){
            throw new BasicException("Testova cast nebyla nalezena");
        }
        templatePart.setName(templatePartSaveDto.getName());
        templatePart.setDescription(templatePartSaveDto.getDescription());
        templatePart.setPoints(templatePartSaveDto.getPoints());
        templatePart.setEstimatedTime(templatePartSaveDto.getEstimatedTime());
        templatePart.setPreferredType(templatePartSaveDto.getPreferredType());
        templatePartDao.update(templatePart);
        return  templatePart;
    }
}
