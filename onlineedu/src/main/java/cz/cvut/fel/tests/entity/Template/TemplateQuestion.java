package cz.cvut.fel.tests.entity.Template;

import cz.cvut.fel.tests.entity.TestTemplatePreferredType;
import cz.cvut.fel.tests.entity.options.AnswerType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class TemplateQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 4000)
    private String content;

    @Enumerated(EnumType.STRING)
    private AnswerType answerType;

    private Double points;

    @ManyToOne
    @JoinColumn(name = "questionGroup", referencedColumnName = "id")
    private TemplateQuestionGroup templateQuestionGroup;

    @OneToMany(mappedBy = "templateQuestion", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TemplatePossibleAnswer> templatePossibleAnswers;
}
