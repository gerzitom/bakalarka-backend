package cz.cvut.fel.tests.dto.user;

import cz.cvut.kotyna.onlineedu.entity.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.registry.infomodel.User;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserReadDto implements Serializable {
    private Integer id;
    private String username;
    private String email;
    private String firstname;
    private String surname;
    private String name;
    private String role;
    private StudentReadDto student;

    public UserReadDto(UserAccount userAccount) {
        this.id = userAccount.getId();
        this.username = userAccount.getUsername();
        this.email = userAccount.getEmail();
        this.firstname = userAccount.getFirstname();
        this.surname = userAccount.getSurname();
        this.role = userAccount.getRole();
        this.name = userAccount.getFullName();
        if(userAccount.getStudent() != null){
            this.student = new StudentReadDto(userAccount.getStudent());
        }
    }


}
