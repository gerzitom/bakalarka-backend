package cz.cvut.fel.tests.dto.Test;

import cz.cvut.fel.tests.dto.RESTTimestampParam;
import cz.cvut.fel.tests.dto.TestSummary.TestSummaryReadDto;
import cz.cvut.fel.tests.entity.Test.Test;
import cz.cvut.fel.tests.entity.Test.TestPart;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TestReadDto {
    private Long id;
    private String name;
    private String startDate;
    private String endDate;
    private List<TestPartReadDto> testParts;
    private List<TestSummaryReadDto> results;
    private List<QuestionGroupReadDto> questionGroups;
    private int duration;


    public TestReadDto(Test entity){
        this.id = entity.getId();
        this.testParts = entity.getTestParts().stream().map(TestPartReadDto::new).collect(Collectors.toList());
        this.results = entity.getTestSummaries().stream().map(TestSummaryReadDto::new).collect(Collectors.toList());
        this.questionGroups = entity.getTestParts().stream()
                .map(TestPart::getQuestionGroup)
                .map(QuestionGroupReadDto::new)
                .collect(Collectors.toList());
        this.duration = entity.getExam().getDuration();
        this.name = entity.getExam().getName();
        this.startDate = new RESTTimestampParam(entity.getExam().getStartDate()).toString();
        this.endDate = new RESTTimestampParam(entity.getExam().getEndDate()).toString();
    }
}
