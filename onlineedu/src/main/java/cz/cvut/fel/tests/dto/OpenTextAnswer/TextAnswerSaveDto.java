package cz.cvut.fel.tests.dto.OpenTextAnswer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class TextAnswerSaveDto {
    private Long questionId;
    private String answer;
}
