package cz.cvut.fel.tests.dto;

import javax.ws.rs.WebApplicationException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
 
public class RESTTimestampParam {
 
    private static final SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss" );
    private Timestamp timestamp;

    public RESTTimestampParam(Date date) {
        this.timestamp = new Timestamp(date.getTime());
    }

    public RESTTimestampParam(String timestampStr ) throws WebApplicationException {
        try {
            timestamp = new Timestamp( df.parse( timestampStr ).getTime() );
        } catch ( final ParseException ex ) {
            throw new WebApplicationException( ex );
        }
    }
 
    public java.sql.Timestamp getTimestamp() {
        return timestamp;
    }
 
    @Override
    public String toString() {
        if ( timestamp != null ) {
            return df.format(timestamp);
        } else {
            return "";
        }
    }
}