package cz.cvut.fel.tests.entity.Test;

import cz.cvut.fel.tests.entity.Template.Template;
import cz.cvut.kotyna.onlineedu.entity.Classroom;
import cz.cvut.kotyna.onlineedu.entity.Student;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NamedQueries(
        @NamedQuery(
                name = Test.QUERY_ALLTESTS,
                query = "SELECT t FROM Test t"
        )
)
public class Test {
    public static final String QUERY_ALLTESTS = "Test.getAllTests";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private boolean teacherMarkingRequired = false;

    @ManyToOne
    @JoinColumn(name = "exam", referencedColumnName = "id")
    private Exam exam;

    @OneToMany(mappedBy = "test", cascade = CascadeType.ALL)
    private List<TestPart> testParts;

    @ManyToOne
    @JoinColumn(name = "classroom", referencedColumnName = "id")
    private Classroom classroom;

    @OneToMany(mappedBy = "test", cascade = CascadeType.ALL)
    private List<TestSummary> testSummaries = new ArrayList<>();

    public TestSummary getStudentSummary(Student student) {
        return getTestSummaries().stream()
                .filter(testSummary -> testSummary.getStudent().equals(student))
                .findFirst()
                .orElse(null);
    }
}
