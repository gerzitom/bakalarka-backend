package cz.cvut.fel.tests.dto.CorrectAndUserAnswers;


import cz.cvut.fel.tests.entity.Test.QuestionGroup;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class RightAnswersDto {
    private Long questionGroupId;
    private List<RightAnswersQuestionsDto> rightAnswersQuestionsDtoList;

    public RightAnswersDto (QuestionGroup entity){
        this.questionGroupId = entity.getId();
        this.rightAnswersQuestionsDtoList = entity.getQuestions().stream().map(RightAnswersQuestionsDto::new).collect(Collectors.toList());
    }

}
