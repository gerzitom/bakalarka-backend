package cz.cvut.fel.tests.service.Test;

import cz.cvut.fel.tests.dao.Test.TestPartDao;
import cz.cvut.fel.tests.entity.Template.TemplateQuestionGroup;
import cz.cvut.fel.tests.entity.Test.QuestionGroup;
import cz.cvut.fel.tests.entity.Test.Test;
import cz.cvut.fel.tests.entity.Test.TestPart;
import cz.cvut.fel.tests.service.Test.QuestionGroupService;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class TestPartService {
    @EJB
    private TestPartDao testPartDao;

    @EJB
    private QuestionGroupService questionGroupService;

    public void createNewTestPart(TemplateQuestionGroup templateQuestionGroup, Test test){
        TestPart testPart = new TestPart();
        testPart.setTest(test);
        QuestionGroup questionGroup = questionGroupService.generateQuestionGroup(templateQuestionGroup);
        testPart.setQuestionGroup(questionGroup);
        testPartDao.save(testPart);
    }
}
