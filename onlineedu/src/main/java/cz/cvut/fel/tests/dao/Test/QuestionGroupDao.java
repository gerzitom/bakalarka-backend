package cz.cvut.fel.tests.dao.Test;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Test.QuestionGroup;

import javax.ejb.Stateless;

@Stateless
public class QuestionGroupDao extends GenericDao<QuestionGroup> {
    public QuestionGroupDao(){
        super(QuestionGroup.class);
    }
}
