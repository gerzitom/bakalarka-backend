package cz.cvut.fel.tests.service.Test;

import cz.cvut.fel.tests.dao.Template.TemplateDao;
import cz.cvut.fel.tests.dao.Test.ExamDao;
import cz.cvut.fel.tests.dto.Test.ExamSaveDto;
import cz.cvut.fel.tests.entity.Test.Exam;
import cz.cvut.fel.tests.entity.Test.Test;
import cz.cvut.fel.tests.entity.Test.TestSummary;
import cz.cvut.fel.tests.exceptions.BasicException;
import cz.cvut.kotyna.onlineedu.entity.Teacher;
import cz.cvut.kotyna.onlineedu.service.UserService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class ExamService {
    @EJB
    private ExamDao examDao;
    @EJB
    private UserService userService;
    @EJB
    private TemplateDao templateDao;
    public Exam createExam(ExamSaveDto dto, Long templateId, int maxPoints){
        Exam exam = new Exam();
        exam.setEndDate(dto.getEndDate());
        exam.setName(dto.getName());
        exam.setStartDate(dto.getStartDate());
        exam.setAuthor(userService.getLoggedInUser().getTeacher());
        exam.setTemplate(templateDao.find(templateId));
        exam.setDuration(dto.getDuration());
        exam.setPoints(maxPoints);
        examDao.save(exam);
        return exam;
    }

    public Exam getExam(Long examId) {
        Exam exam = examDao.find(examId);
        if(exam == null){
            throw new BasicException("Tato zkouska neexistuje");
        }
        return exam;
    }

    public Exam updateExam(ExamSaveDto dto, Long examId) {
        Exam exam = examDao.find(examId);
        if(exam == null){
            throw new BasicException("Tato zkouska neexistuje");
        }
        exam.setEndDate(dto.getEndDate());
        exam.setName(dto.getName());
        exam.setStartDate(dto.getStartDate());
        exam.setAuthor(userService.getLoggedInUser().getTeacher());
        examDao.update(exam);
        return exam;
    }

    public void deleteExam(Long examId) {
        if(examDao.find(examId) == null){
            throw new BasicException("Zkouska nebyla nalezena");
        }
        examDao.delete(examId);
    }

    public List<Exam> getAllExams() {
        Teacher teacher = userService.getLoggedInUser().getTeacher();
        if(teacher == null){
            throw new BasicException("Nebyl nalezen zadny ucitel");
        }
        return teacher.getExamList();
    }

    public List<TestSummary> getTests(Long examId) {
        Exam exam = examDao.find(examId);
        List<TestSummary> testSummaries = new ArrayList<>();
        List<Test> tests = exam.getTests();
        for(Test t: tests){
            testSummaries.addAll(t.getTestSummaries());
        }
        List<TestSummary> summaries = new ArrayList<>();
        for(TestSummary t : testSummaries){
            summaries.add(t);
        }
        return summaries;
    }
}
