package cz.cvut.fel.tests.controller;

import cz.cvut.fel.tests.dto.ClassRoomSaveDto;
import cz.cvut.fel.tests.dto.ClassroomReadDto;
import cz.cvut.fel.tests.service.ClassRoomServiceTests;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

@Path("/classrooms")
public class ClassroomController {
    @EJB
    private ClassRoomServiceTests classroomServiceTests;

    @GET
    public List<ClassroomReadDto> getAllClassRooms() {
        return classroomServiceTests.getAllClassRooms();
    }

    @POST
    public ClassroomReadDto createClassroom(ClassRoomSaveDto dto) {
        return classroomServiceTests.createClassroom(dto);
    }

    @GET
    @Path("/{classroomId}")
    public ClassroomReadDto getClassroom(
            @PathParam("classroomId") Long id
    ) {
        return classroomServiceTests.getClassRoom(id);
    }

    @POST
    @Path("/{classroomId}/{studentId}")
    public ClassroomReadDto addStudent(
            @PathParam("classroomId") Long classId,
            @PathParam("studentId") Integer studentId
    ){
        return classroomServiceTests.addStudent(classId, studentId);
    }
    



}
