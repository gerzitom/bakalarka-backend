package cz.cvut.fel.tests.entity.Test;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class QuestionResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "testSummary", referencedColumnName = "id")
    private TestSummary testSummary;

    @ManyToOne
    @JoinColumn(name = "question", referencedColumnName = "id")
    private Question question;

    @ManyToMany
    @JoinTable(name="answers",
            joinColumns =
            @JoinColumn(name="resultId", referencedColumnName="id"),
            inverseJoinColumns=
            @JoinColumn(name="possibleAnswerId", referencedColumnName="id")
    )
    private List<PossibleAnswer> answers;
}
