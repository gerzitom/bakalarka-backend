package cz.cvut.fel.tests.dto.Template.Part;

import cz.cvut.fel.tests.dto.Template.QuestionGroup.TemplateQuestionGroupReadDto;
import cz.cvut.fel.tests.entity.Template.TemplatePart;
import cz.cvut.fel.tests.entity.TestTemplatePreferredType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TemplatePartReadDto {
    private Long id;
    private String name;
    private int estimatedTime;
    private String description;
    private int points;
    private List<TemplateQuestionGroupReadDto> questionGroups;
    private TestTemplatePreferredType preferredType;

    public TemplatePartReadDto(TemplatePart entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        this.estimatedTime = entity.getEstimatedTime();
        this.description = entity.getDescription();
        this.questionGroups = entity.getTemplateQuestionGroups().stream().map(TemplateQuestionGroupReadDto::new).collect(Collectors.toList());
        this.preferredType = entity.getPreferredType();
        this.points = entity.getPoints();
    }
}
