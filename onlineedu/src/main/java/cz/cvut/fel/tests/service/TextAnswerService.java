package cz.cvut.fel.tests.service;


import cz.cvut.fel.tests.dao.Test.QuestionDao;
import cz.cvut.fel.tests.dao.Test.TestSummaryDao;
import cz.cvut.fel.tests.dao.TextAnswerDao;
import cz.cvut.fel.tests.dto.OpenTextAnswer.TextAnswerEvaluateDto;
import cz.cvut.fel.tests.dto.OpenTextAnswer.TextAnswerReadDto;
import cz.cvut.fel.tests.entity.Template.TextAnswer;
import cz.cvut.fel.tests.entity.Test.Question;
import cz.cvut.fel.tests.entity.Test.TestSummary;
import cz.cvut.fel.tests.exceptions.BasicException;
import liquibase.pro.packaged.E;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class TextAnswerService {
    @EJB
    private TextAnswerDao textAnswerDao;
    @EJB
    private QuestionDao questionDAo;
    @EJB
    private TestSummaryDao testSummaryDao;


    public TextAnswerReadDto evaluateTextAnswer(Long id, TextAnswerEvaluateDto dto){
        TextAnswer textAnswer = textAnswerDao.find(id);
        double maxPoints = textAnswer.getQuestion().getPoints();
        if(dto.getPoints() > maxPoints){
            throw new BasicException("Maximální počet bodů je " + maxPoints);
        }
        textAnswer.setPointsReceived(dto.getPoints());
        TestSummary testSummary = textAnswer.getQuestion().getQuestionGroup().getTestParts().get(0).getTest().getTestSummaries().get(0);
        if(Boolean.TRUE.equals(textAnswer.getMarked())){
            throw new BasicException("Tuto otázku jste již ohodnotili");
        }
        double points = testSummary.getAmountOfPoints();
        points += dto.getPoints();
        testSummary.setAmountOfPoints(points);
        textAnswer.setMarked(true);
        testSummaryDao.update(testSummary);
        textAnswerDao.update(textAnswer);
        return new TextAnswerReadDto(textAnswer);
    }

    public TextAnswer getAnswer(Long id) {
        return textAnswerDao.find(id);
    }

    public List<TextAnswer> getAnswerFromQuestion(Long id) {
        Question  question = questionDAo.find(id);
        return question.getTextAnswers();
    }
}
