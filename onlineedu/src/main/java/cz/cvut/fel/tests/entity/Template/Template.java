package cz.cvut.fel.tests.entity.Template;

import cz.cvut.fel.tests.entity.Tag;
import cz.cvut.fel.tests.entity.Test.Exam;
import cz.cvut.kotyna.onlineedu.entity.Teacher;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NamedQueries({
    @NamedQuery(
        name = Template.QUERY_FINDBYAUTHOR,
        query = "SELECT t FROM Template t WHERE t.author.id = :authorId"
    ),
    @NamedQuery(
        name = Template.QUERY_FINDBYNAME,
        query = "SELECT t FROM Template t WHERE t.name = :name"
    )
})
public class Template {

    public static final String QUERY_FINDBYAUTHOR = "TestTemplate.findByAuthor";
    public static final String QUERY_FINDBYNAME = "TestTemplate.findByName";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "author", referencedColumnName = "id")
    private Teacher author;

    @OneToMany(mappedBy = "template", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<TemplatePart> templateParts;

    @ManyToMany
    private List<Tag> tags;

    @OneToMany(mappedBy = "template")
    private List<Exam> exams;
}
