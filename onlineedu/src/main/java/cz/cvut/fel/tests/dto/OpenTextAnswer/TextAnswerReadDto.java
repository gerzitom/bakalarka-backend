package cz.cvut.fel.tests.dto.OpenTextAnswer;

import cz.cvut.fel.tests.dto.Test.QuestionReadDto;
import cz.cvut.fel.tests.entity.Template.TextAnswer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TextAnswerReadDto {
    private Long id;
    private String answer;
    private Boolean marked;
    private Double points;
    private QuestionReadDto questionReadDto;

    public TextAnswerReadDto(TextAnswer entity){
        this.id = entity.getId();
        this.answer = entity.getAnswer();
        this.marked = entity.getMarked();
        this.points = entity.getPointsReceived();
        this.questionReadDto = new QuestionReadDto(entity.getQuestion());
    }
}
