package cz.cvut.fel.tests.service.Test;

import cz.cvut.fel.tests.controller.Websocket.SubmittedTestsWS;
import cz.cvut.fel.tests.dao.Template.QuestionResultDao;
import cz.cvut.fel.tests.dao.Test.*;
import cz.cvut.fel.tests.dao.TextAnswerDao;
import cz.cvut.fel.tests.dto.AnswerSaveDto;
import cz.cvut.fel.tests.dto.CorrectAndUserAnswers.TestAnswers;
import cz.cvut.fel.tests.dto.CorrectAndUserAnswers.UserAnswersDto;
import cz.cvut.fel.tests.dto.OpenTextAnswer.TextAnswerSaveDto;
import cz.cvut.fel.tests.dto.Test.TestReadDto;
import cz.cvut.fel.tests.entity.GroupType;
import cz.cvut.fel.tests.entity.Template.TextAnswer;
import cz.cvut.fel.tests.entity.Test.*;
import cz.cvut.fel.tests.entity.options.AnswerType;
import cz.cvut.fel.tests.exceptions.BasicException;
import cz.cvut.kotyna.onlineedu.entity.Student;
import cz.cvut.kotyna.onlineedu.service.UserService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.websocket.EncodeException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Stateless
public class TestService {
    @EJB
    private TestDao testDao;

    @EJB
    private QuestionDao questionDao;

    @EJB
    private PossibleAnswerDao possibleAnswerDao;

    @EJB
    private QuestionResultDao questionResultDao;

    @EJB
    private TestSummaryDao testSummaryDao;

    @EJB
    private TextAnswerDao textAnswerDao;

    @EJB
    private UserService userService;

    @EJB
    private ExamDao examDao;

    @Inject
    private SubmittedTestsWS submittedTestsWS;


    public List<Test> getAllTests(){
        Student student = userService.getLoggedInUser().getStudent();
        if(student == null){
            throw new BasicException("Zak nebyl nalezen");
        }
        List<TestSummary> testSummaries = student.getTestSummaries();
        if(testSummaries.isEmpty()){
            throw new BasicException("Žák nemá přiřazeny žádné testy");
        }
        List<Test> tests = new ArrayList<>();
        for(TestSummary t : testSummaries){
            tests.add(t.getTest());
        }
        return tests;
    }

    public TestReadDto getTest(Long testId) {
        return new TestReadDto(testDao.find(testId));
    }

    public Test createTest(Exam exam){
        Test test = new Test();
        test.setExam(exam);
        testDao.save(test);
        return test;
    }

    public List<TestPart> getTestParts(Test test){
        return test.getTestParts();
    }

    public TestReadDto updateDto(Test test){
        return new TestReadDto(test);
    }

    public List<TestPart> getTestParts(Long testId){
        Test test = testDao.find(testId);
        if(test == null){
            throw new BasicException("Test nebyl nalezen");
        }
        return test.getTestParts();
    }

    /**
     * Saves an answer to the MULTIPLEOPTIONS and SINGLEOPTION type questions.
     * The method checks if the user is trying to save answers in the time window specified by
     * StartDate and EndDate parameters of the parent entity Exam.
     * @param testId path parameter with the id of the specific test
     * @param answerSaveDto DTO which includes a Question id and the ids of the answers
     */
    public void saveAnswerToQuestionResult(Long testId, AnswerSaveDto answerSaveDto){
        Question question = questionDao.find(answerSaveDto.getQuestionId());
        if(question == null) throw new BasicException("Otázka nebyla nalezena");
        Test test = testDao.find(testId);
        if(test == null) throw new BasicException("Test nebyl nalezen");
        Date currentDate = new Date();
        if(currentDate.before(test.getExam().getStartDate())){
            throw new BasicException("Počkejte, než budete vpuštěni do testu.");
        }
        TestSummary testSummary = test.getTestSummaries().get(0);
        if(testSummary.getTimeFinished() != null){
            throw new BasicException("Test byl ukončen, již nelze odpovídat na otázky.");
        }
        if(currentDate.after(test.getExam().getEndDate())){
            throw new BasicException("Test byl uzavřen.");
        }
        QuestionResult questionResult;
        if(question.getQuestionResults().isEmpty()){
            questionResult = new QuestionResult();
        } else {
            questionResult = question.getQuestionResults().get(0);
        }
        questionResult.setQuestion(question);
        List<PossibleAnswer> answers = new ArrayList<>();
        for(Long i : answerSaveDto.getAnswerIds()){
            answers.add(possibleAnswerDao.find(i));
            possibleAnswerDao.update(possibleAnswerDao.find(i));
        }
        questionResult.setAnswers(answers);
        questionResult.setTestSummary(test.getTestSummaries().get(0));
        questionResultDao.save(questionResult);
    }


    /**
     * Calculates the amount of points for the SINGLE_OPTION and MULTIPLEOPTIONS type
     * @param testId the test which the user wants to evaluate
     * @return update Test entity
     */
    public Test evaluateTest(Long testId) {
        Test test = testDao.find(testId);
        double numberOfPoints = 0;
        if(test == null){
            throw new BasicException("Test nebyl nalezen");
        }
        TestSummary testSummary = test.getTestSummaries().get(0);
        List<QuestionResult> questionResults = testSummary.getQuestionResults();
        for(QuestionResult q : questionResults){
           List<PossibleAnswer> answers = q.getAnswers();
           if(q.getQuestion().getAnswerType() == AnswerType.OPENANSWER){
                test.setTeacherMarkingRequired(true);
           }
           for (PossibleAnswer p : answers){
               double pointsForQuestion = p.getQuestion().getPoints();
               if(q.getQuestion().getQuestionGroup().getGroupType().equals(GroupType.SPOJOVACKA)){
                   if(Objects.equals(q.getQuestion().getPossibleAnswers().get(0).getId(), p.getId())){
                       numberOfPoints += pointsForQuestion;
                   }
               } else {
                   if(Boolean.TRUE.equals(p.getCorrect()) &&
                           p.getQuestion().getAnswerType() == AnswerType.ONE_OPTION){
                       numberOfPoints += pointsForQuestion;
                   }
                   if(Boolean.TRUE.equals(p.getCorrect()) &&
                           p.getQuestion().getAnswerType() == AnswerType.MULTIPLE_OPTIONS){
                       List<PossibleAnswer> allOptions = p.getQuestion().getPossibleAnswers();
                       numberOfPoints += pointsForQuestion/allOptions.size();
                   }
               }
           }
        }
        Date date = new Date(System.currentTimeMillis());
        testSummary.setTimeFinished(date);
        testSummary.setAmountOfPoints(numberOfPoints);
        testSummaryDao.update(testSummary);

        try {
            submittedTestsWS.testSubmitted(test.getExam().getId());
        } catch (Exception e) {
            System.out.println(e);
        }
        return test;
    }



    public TestAnswers getTestAnswers(Long testId) {
        Test test = testDao.find(testId);
        if(test == null) throw new BasicException("Test nenalezen");
        return new TestAnswers(test);
    }

    public Test startTest(Long testId) {
        Test test = testDao.find(testId);
        TestSummary testSummary = test.getTestSummaries().get(0);
        Date date = new Date(System.currentTimeMillis());
        Date startDate = test.getExam().getStartDate();
        if(date.before(test.getExam().getStartDate())){
            throw new BasicException("Test můžete zapnout až po " + startDate);
        }
        testSummary.setTimeStarted(date);
        submittedTestsWS.testSubmitted(test.getExam().getId());
        return test;
    }

    public TextAnswer saveAnswerToOpenQuestion(Long testId, TextAnswerSaveDto answerSaveDto) {
        Question question = questionDao.find(answerSaveDto.getQuestionId());
        if(question == null){
            throw new BasicException("Otazka nebyla nalezena");
        }
        if(question.getAnswerType() != AnswerType.OPENANSWER){
            throw new BasicException("Na tuto otazku nelze odpovedet textovou odpovedi");
        }
        TextAnswer textAnswer;
        if(question.getTextAnswers().isEmpty()){
             textAnswer = new TextAnswer();
        } else {
            textAnswer = question.getTextAnswers().get(0);
        }
        textAnswer.setAnswer(answerSaveDto.getAnswer());
        textAnswer.setQuestion(question);
        textAnswerDao.save(textAnswer);
        return textAnswer;
    }

    public List<TextAnswer> getAllTextAnswers(Long id) {
        Test test = testDao.find(id);
        List<TestPart> testParts = test.getTestParts();
        List<QuestionGroup> questionGroups = new ArrayList<>();
        List<Question> questions = new ArrayList<>();
        List<TextAnswer> textAnswers = new ArrayList<>();
        for(TestPart t : testParts){
            questionGroups.add(t.getQuestionGroup());
        }
        for(QuestionGroup questionGroup : questionGroups){
            questions.addAll(questionGroup.getQuestions());
        }
        for(Question q : questions){
            textAnswers.addAll(q.getTextAnswers());
        }
        return textAnswers;
    }

    public List<TestSummary> getSubmittedTests(Long id) {
        Exam exam = examDao.find(id);
        List<TestSummary> testSummaries = new ArrayList<>();
        List<Test> tests = exam.getTests();
        for(Test t: tests){
            testSummaries.addAll(t.getTestSummaries());
        }
        List<TestSummary> submitted = new ArrayList<>();
        for(TestSummary t : testSummaries){
            Date timeFinished = t.getTimeFinished();
            if(timeFinished != null){
                submitted.add(t);
            }
        }
        return submitted;
    }

    public String deleteTest(Long testId) {
        if(testDao.find(testId) == null) throw new BasicException("Test nebyl nalezen");
        testDao.delete(testId);
        return "Test byl smazan";
    }

    public List<QuestionGroup> getUserAnswers(Long id) {
        Test test = testDao.find(id);
        List<TestPart> testParts = test.getTestParts();
        List<QuestionGroup> questionGroups = new ArrayList<>();
        for(TestPart t : testParts){
            questionGroups.add(t.getQuestionGroup());
        }
        return questionGroups;
    }
}
