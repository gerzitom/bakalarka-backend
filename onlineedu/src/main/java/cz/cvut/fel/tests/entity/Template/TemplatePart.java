package cz.cvut.fel.tests.entity.Template;

import cz.cvut.fel.tests.entity.TestTemplatePreferredType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
public class TemplatePart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private int estimatedTime;
    private Integer points;

    @Enumerated(EnumType.STRING)
    private TestTemplatePreferredType preferredType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "template", referencedColumnName = "id")
    private Template template;

    @OneToMany(mappedBy = "templatePart", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TemplateQuestionGroup> templateQuestionGroups;
}
