package cz.cvut.fel.tests.dto.Test;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExamSaveDto {
    private String name;
    private Integer classroomId;
    private Date startDate;
    private Date endDate;
    private int duration;
}
