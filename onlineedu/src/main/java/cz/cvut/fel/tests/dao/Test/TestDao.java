package cz.cvut.fel.tests.dao.Test;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Test.Test;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class TestDao extends GenericDao<Test> {
    @PersistenceContext
    EntityManager em;

    public TestDao() {
        super(Test.class);
    }

    public List<Test> getAllTests(){
        return em.createNamedQuery(Test.QUERY_ALLTESTS, Test.class)
                .getResultList();
    }
}
