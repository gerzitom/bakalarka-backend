package cz.cvut.fel.tests.exceptions;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.ws.rs.core.Response;

@ThrowException
@Interceptor
public class ExceptionInterceptor {
    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {
        try {
            return context.proceed();
        } catch (BasicException ex) {
            return Response
                    .ok(ex.getDto())
                    .status(ex.getCode())
                    .build();
        }
    }
}