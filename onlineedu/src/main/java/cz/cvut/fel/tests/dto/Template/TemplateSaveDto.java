package cz.cvut.fel.tests.dto.Template;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TemplateSaveDto {
    private String name;
}
