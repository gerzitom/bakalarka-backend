package cz.cvut.fel.tests.dto.Template;

import cz.cvut.fel.tests.dto.Template.Part.TemplatePartReadDto;
import cz.cvut.fel.tests.entity.Template.Template;
import cz.cvut.fel.tests.entity.Template.TemplatePart;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TemplateReadDto {
    private Long id;
    private String name;
    private Integer authorId;
    private int recommendedTime;
    private List<TemplatePartReadDto> parts;

    public TemplateReadDto(Template entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        this.authorId = entity.getAuthor().getId();
        this.parts = entity.getTemplateParts().stream().map(TemplatePartReadDto::new).collect(Collectors.toList());
        this.recommendedTime = entity.getTemplateParts()
                .stream()
                .map(TemplatePart::getEstimatedTime)
                .reduce(0, Integer::sum);
    }
}
