package cz.cvut.fel.tests.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestSummaryDto {
    private Date timeStarted;
    private Date timeFinished;
    private int numberOfPoints;

}
