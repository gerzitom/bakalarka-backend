package cz.cvut.fel.tests.dto.Websocket;

import javax.json.bind.JsonbBuilder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class TestCompleteInfromationEncoder implements Encoder.Text<TestCompleteInformation> {

@Override
public String encode(TestCompleteInformation testCompleteInformation) throws EncodeException {
        //Using JSON-B (JSR 367) API for mapping to JSON from T
        return JsonbBuilder.create().toJson(testCompleteInformation);

        }

@Override
public void init(EndpointConfig endpointConfig) {
        }

@Override
public void destroy() {
        }
}
