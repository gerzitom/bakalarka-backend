package cz.cvut.fel.tests.service.Template;

import cz.cvut.fel.tests.dao.Template.TemplateDao;
import cz.cvut.fel.tests.dao.Template.TemplatePartDao;
import cz.cvut.fel.tests.dao.Template.TemplateQuestionDao;
import cz.cvut.fel.tests.dao.Template.TemplateQuestionGroupDao;
import cz.cvut.fel.tests.dto.Template.Part.TemplatePartReadDto;
import cz.cvut.fel.tests.dto.Template.TemplateReadDto;
import cz.cvut.fel.tests.dto.Template.TemplateSaveDto;
import cz.cvut.fel.tests.entity.Template.Template;
import cz.cvut.fel.tests.entity.Template.TemplatePart;
import cz.cvut.fel.tests.entity.Template.TemplateQuestionGroup;
import cz.cvut.fel.tests.entity.Test.Exam;
import cz.cvut.fel.tests.exceptions.BasicException;
import cz.cvut.kotyna.onlineedu.entity.Teacher;
import cz.cvut.kotyna.onlineedu.entity.UserAccount;
import cz.cvut.kotyna.onlineedu.service.TeacherService;
import cz.cvut.kotyna.onlineedu.service.UserService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class TemplateService {
    @EJB
    private TemplateDao templateDao;

    @EJB
    private TemplatePartDao templatePartDao;

    @EJB
    private TeacherService teacherService;

    @EJB
    private UserService userService;

    @EJB
    private TemplateQuestionGroupDao templateQuestionGroupDao;
    @EJB
    private TemplateQuestionDao templateQuestionDao;




    public List<Template> getTestTemplates(){
        UserAccount loggedInUser = userService.getLoggedInUser();
        return templateDao.getTemplatesByAuthor(Long.valueOf(loggedInUser.getTeacher().getId()));
    }

    public List<TemplateReadDto> getTestTemplates(Long authorId){
        return templateDao.getTemplatesByAuthor(authorId)
                .stream().map(TemplateReadDto::new)
                .collect(Collectors.toList());
    }

    public Template saveTestTemplate(TemplateSaveDto dto){
        Teacher teacher = userService.getLoggedInUser().getTeacher();
        Template foundTemplate = templateDao.getTemplatesByName(dto.getName()).stream().findFirst().orElse(null);
        if(foundTemplate != null) throw new BasicException("Šablona se stejným názvem už existuje.");
        Template newTemplate = templateDao.save(toEntityFromDto(dto));
        newTemplate.setAuthor(teacher);
        return newTemplate;
    }

    public Template updateTestTemplate(TemplateSaveDto dto, Long id){
        Template template = templateDao.find(id);
        template.setName(dto.getName());
        return template;
    }

    private Template toEntityFromDto(TemplateSaveDto dto){
        Template template = new Template();
        template.setName(dto.getName());
        Teacher teacher = userService.getLoggedInUser().getTeacher();
        template.setAuthor(teacher);
        return template;
    }

    public Template getTestTemplate(Long templateId){
        Template template = templateDao.find(templateId);
        if(template == null){
            throw new BasicException("Testový vzor nebyl nalezen");
        }
        return template;
    }

    public List<TemplatePart> getTestParts(Long templateId){
        Template template = templateDao.find(templateId);
        return template.getTemplateParts();
    }

    public List<TemplatePart> getTestReadParts(Long templateId) {
        Template template = templateDao.find(templateId);
        return template.getTemplateParts();
    }

    public TemplatePartReadDto getTestPartQuestions(Long partId) {
        TemplatePart part = templatePartDao.find(partId);
        return new TemplatePartReadDto(part);
    }

    public void deleteTestTemplate(Long id) {
        Template template = templateDao.find(id);
        if(template == null){
            throw new BasicException("Testovy vzor nebyl nalezen");
        }
        List<Exam> exams = template.getExams();
        for(Exam e : exams){
            e.setTemplate(null);
        }
        templateDao.delete(id);
    }

    public TemplateQuestionGroup removeQuestionGroup(Long groupId) {
        TemplateQuestionGroup templateQuestionGroup = templateQuestionGroupDao.find(groupId);
        templateQuestionGroup.setTemplatePart(null);
        templateQuestionGroupDao.update(templateQuestionGroup);
        return templateQuestionGroup;
    }
}
