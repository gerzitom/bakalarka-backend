package cz.cvut.fel.tests.dao.Template;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Template.TemplatePossibleAnswer;

import javax.ejb.Stateless;

@Stateless
public class TemplatePossibleAnswerDao extends GenericDao<TemplatePossibleAnswer> {
    public TemplatePossibleAnswerDao(){
        super(TemplatePossibleAnswer.class);
    }
}
