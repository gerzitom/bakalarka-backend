package cz.cvut.fel.tests.dto.TestSummary;

import cz.cvut.fel.tests.dto.Template.Option.TemplateOptionReadDto;
import cz.cvut.fel.tests.entity.Template.TemplatePossibleAnswer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PossibleAnswerReadDtoSummary {
    private Long id;
    private TemplateOptionReadDto singleOption;
    private boolean isCorrect;

    public PossibleAnswerReadDtoSummary(TemplatePossibleAnswer entity){
        this.id = entity.getId();
        this.singleOption = new TemplateOptionReadDto(entity.getOption());
        this.isCorrect = entity.getCorrect();
    }
}
