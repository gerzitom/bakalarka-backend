package cz.cvut.fel.tests.entity.Template;

import cz.cvut.fel.tests.entity.Test.Question;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class TextAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String answer;
    private Boolean marked;
    private Double pointsReceived;

    @ManyToOne
    @JoinColumn(name = "question", referencedColumnName = "id")
    private Question question;

}
