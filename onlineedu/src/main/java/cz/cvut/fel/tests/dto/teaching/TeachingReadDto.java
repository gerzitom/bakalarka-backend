package cz.cvut.fel.tests.dto.teaching;

import cz.cvut.fel.tests.dto.ClassroomReadDto;
import cz.cvut.fel.tests.dto.user.UserReadDto;
import cz.cvut.kotyna.onlineedu.entity.Classroom;
import cz.cvut.kotyna.onlineedu.entity.Teacher;
import cz.cvut.kotyna.onlineedu.entity.Teaching;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeachingReadDto {
    private Integer id;
    private ClassroomReadDto classroom;
    private UserReadDto teacher;

    public TeachingReadDto (Teaching entity){
        this.id  = entity.getId();
        this.classroom = new ClassroomReadDto(entity.getClassroom());
        this.teacher = new UserReadDto(entity.getTeacher().getUserAccount());
    }
}
