package cz.cvut.fel.tests.entity.Test;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class PossibleAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Boolean correct;

    @ManyToOne(cascade=CascadeType.REMOVE)
    @JoinColumn(name = "option", referencedColumnName = "id")
    private Option option;


    @ManyToOne
    @JoinColumn(name = "question", referencedColumnName = "id")
    private Question question;

    @ManyToMany(mappedBy = "answers", fetch= FetchType.EAGER, cascade=CascadeType.ALL)
    List<QuestionResult> questionResults;

    public PossibleAnswer(Long id, Boolean correct, Option option, Question question) {
        this.id = id;
        this.correct = correct;
        this.option = option;
        this.question = question;
    }
}
