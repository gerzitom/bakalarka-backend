package cz.cvut.fel.tests.dto.Test;


import cz.cvut.fel.tests.entity.GroupType;
import cz.cvut.fel.tests.entity.Test.QuestionGroup;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class QuestionGroupReadDto {
    private Long id;
    private String instructions;
    private GroupType groupType;
    private Long partId;
    private List<QuestionReadDto> questions;

    public QuestionGroupReadDto(QuestionGroup entity) {
        this.id = entity.getId();
        this.instructions = entity.getInstructions();
        this.groupType = entity.getGroupType();
        this.questions = entity.getQuestions().stream().map(QuestionReadDto::new).collect(Collectors.toList());
    }
}
