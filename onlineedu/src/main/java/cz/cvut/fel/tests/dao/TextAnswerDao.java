package cz.cvut.fel.tests.dao;

import cz.cvut.fel.tests.entity.Template.TextAnswer;

import javax.ejb.Stateless;

@Stateless
public class TextAnswerDao extends GenericDao<TextAnswer> {
    public TextAnswerDao() {
        super(TextAnswer.class);
    }
}
