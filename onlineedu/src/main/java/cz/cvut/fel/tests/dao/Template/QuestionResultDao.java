package cz.cvut.fel.tests.dao.Template;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Test.QuestionResult;

import javax.ejb.Stateless;

@Stateless
public class QuestionResultDao extends GenericDao<QuestionResult> {
    public QuestionResultDao() { super(QuestionResult.class);}
}
