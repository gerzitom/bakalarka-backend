package cz.cvut.fel.tests.entity.options;

public enum AnswerType {
    ONE_OPTION, MULTIPLE_OPTIONS, OPENANSWER
}
