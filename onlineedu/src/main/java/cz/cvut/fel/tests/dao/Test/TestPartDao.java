package cz.cvut.fel.tests.dao.Test;

import cz.cvut.fel.tests.dao.GenericDao;
import cz.cvut.fel.tests.entity.Test.TestPart;

import javax.ejb.Stateless;

@Stateless
public class TestPartDao extends GenericDao<TestPart> {
    public TestPartDao(){
        super(TestPart.class);
    }
}
