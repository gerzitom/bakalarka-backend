package cz.cvut.fel.tests.entity.Test;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class TestPart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "test", referencedColumnName = "id")
    private Test test;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "questionGroup", referencedColumnName = "id")
    private QuestionGroup questionGroup;


}
