package cz.cvut.fel.tests.controller;

import cz.cvut.fel.tests.dto.teaching.TeachingReadDto;
import cz.cvut.fel.tests.dto.teaching.TeachingSaveDto;
import cz.cvut.fel.tests.service.TeachingServiceTests;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

@Path("/teaching")
public class TeachingController {
    @EJB
    private TeachingServiceTests teachingServiceTests;

    @GET
    @Path("/{teachingId}")
    public TeachingReadDto getTeaching(
            @PathParam("teachingId") Integer id
    ){
        return teachingServiceTests.getTeaching(id);
    }

    @GET
    @Path("/allTeachings")
    public List<TeachingReadDto> getTeachingCollection (Integer id){
        return teachingServiceTests.getTeachingCollection(id);
    }

    @POST
    public TeachingReadDto createTeaching(TeachingSaveDto dto){
        return teachingServiceTests.createTeaching(dto);
    }

    @PUT
    @Path("/{teachingId}")
    public TeachingReadDto updateTeaching(
            @PathParam("teachingId") Integer id,
            TeachingSaveDto dto
    ){
        return teachingServiceTests.updateTeaching(id, dto);
    }

}
