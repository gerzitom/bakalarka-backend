package cz.cvut.fel.tests.dto.Websocket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TestCompleteInformation {
    boolean testFinished;
}
