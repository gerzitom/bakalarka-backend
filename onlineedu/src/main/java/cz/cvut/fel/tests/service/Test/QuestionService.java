package cz.cvut.fel.tests.service.Test;

import cz.cvut.fel.tests.dao.Test.QuestionDao;
import cz.cvut.fel.tests.entity.Template.TemplatePossibleAnswer;
import cz.cvut.fel.tests.entity.Template.TemplateQuestion;
import cz.cvut.fel.tests.entity.Test.Question;
import cz.cvut.fel.tests.entity.Test.QuestionGroup;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class QuestionService {
    @EJB
    private PossibleAnswerService possibleAnswerService;
    @EJB
    private QuestionDao questionDao;
    public void generateTestQuestion(TemplateQuestion templateQuestion, QuestionGroup questionGroup){
        Question question = generateTestQuestionFromQuestion(templateQuestion);
        List<TemplatePossibleAnswer> templatePossibleAnswers = templateQuestion.getTemplatePossibleAnswers();
        for(TemplatePossibleAnswer p : templatePossibleAnswers){
            possibleAnswerService.generatePossibleAnswer(p, question);
        }
        question.setQuestionGroup(questionGroup);
        questionDao.save(question);
    }

    public Question generateTestQuestionFromQuestion(TemplateQuestion templateQuestion){
        Question question = new Question(
                templateQuestion.getId(), templateQuestion.getContent(),
                templateQuestion.getAnswerType(), templateQuestion.getPoints());
        questionDao.save(question);
        return question;
    }
}
